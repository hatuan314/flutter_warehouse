import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:warehouse_of_pikata/ui/view/register/sign_in_view.dart';

import 'background_screen.dart';

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => SplashScreenState();
}

class SplashScreenState extends State<SplashScreen> {
  Future initData() async {
    await Future.delayed(Duration(seconds: 3));
  }

  Future navigateToScreen() async {
    Navigator.pushReplacement(
        context, CupertinoPageRoute(builder: (context) => SignInView()));
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initData().then((value) async {
      await navigateToScreen();
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    ScreenUtil.init(context, width: 375, height: 812, allowFontScaling: true);
    return Scaffold(
      body: Container(
        child: FancyBackgroundApp(messege: "Chào mẹ!"),
      ),
    );
  }
}
