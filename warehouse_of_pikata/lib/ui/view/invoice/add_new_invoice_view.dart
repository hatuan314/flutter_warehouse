import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rounded_date_picker/rounded_picker.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:toast/toast.dart';
import 'package:warehouse_of_pikata/enums/view_state.dart';
import 'package:warehouse_of_pikata/models/invoice.dart';
import 'package:warehouse_of_pikata/scoped_models/invoices/invoice_view_model.dart';
import 'package:warehouse_of_pikata/ui/view/base_view.dart';
import 'package:warehouse_of_pikata/ui/view/distributor/add_new_distributor_view.dart';
import 'package:warehouse_of_pikata/ui/view/invoice/product_element_view.dart';
import 'package:warehouse_of_pikata/ui/widget/base_widget.dart';
import 'package:warehouse_of_pikata/ui/widget/loading/loading_view.dart';
import 'package:warehouse_of_pikata/utils.dart';

class AddNewInvoiceView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _AddNewInvoiceViewState();
}

class _AddNewInvoiceViewState extends State<AddNewInvoiceView> {
  GlobalKey<FormState> textFormKey = GlobalKey<FormState>();

  List<Widget> _allProductsInvoiceWidget = [];

  @override
  void initState() {
    // TODO: implement initState

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    ScreenUtil.init(context, width: 375, height: 812, allowFontScaling: true);
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [Colors.blue[900], Colors.red[900]],
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          tileMode: TileMode.clamp,
        ),
      ),
      child: BaseView<InvoiceViewModel>(onModelReady: (model) {
        model.getAllDistributors();
        model.getAllProducts();
      }, builder: (context, child, model) {
        switch (model.getProductsState) {
          case ViewState.Success:
            if (_allProductsInvoiceWidget == null ||
                _allProductsInvoiceWidget.length == 0) {
              _allProductsInvoiceWidget
                  .add(ProductElementView(model: model, index: 0));
            }
            return Scaffold(
              backgroundColor: Colors.transparent,
              appBar: BaseWidget.appBar('Nhập hoá đơn'),
              body: Form(
                key: textFormKey,
                child: Stack(
                  alignment: Alignment.bottomCenter,
                  children: <Widget>[
                    SizedBox(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height,
                    ),
                    Container(
                      decoration: BoxDecoration(
                          borderRadius:
                              BorderRadius.vertical(top: Radius.circular(15)),
                          color: Colors.grey[100]),
                      padding: EdgeInsets.symmetric(
                          horizontal: ScreenUtil().setWidth(10),
                          vertical: ScreenUtil().setHeight(8)),
                      child: CustomScrollView(
                        shrinkWrap: true,
                        slivers: <Widget>[
                          SliverToBoxAdapter(
                            child: _dateWidget(model),
                          ),
                          SliverToBoxAdapter(
                            child: _distributorComboBox(model),
                          ),
                          SliverToBoxAdapter(
                            child: _productsInvoice(model),
                          ),
                          SliverToBoxAdapter(
                            child: _funcButton(model),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            );
          case ViewState.Busy:
            return Scaffold(
              backgroundColor: Colors.transparent,
              appBar: BaseWidget.appBar('Nhập hoá đơn'),
              body: Center(
                child: LoadingView(),
              ),
            );
          default:
            return Scaffold(
              backgroundColor: Colors.transparent,
              appBar: BaseWidget.appBar('Nhập hoá đơn'),
              body: Center(
                child: BaseWidget.text('Lỗi', textColor: Colors.white),
              ),
            );
        }
      }),
    );
  }

  Column _productsInvoice(InvoiceViewModel model) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        BaseWidget.text('Danh sách sản phẩm', fontWeight: FontWeight.w700),
        SizedBox(height: ScreenUtil().setHeight(10)),
        Container(
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(10)),
              border: Border.all(
                  color: Colors.blue, width: ScreenUtil().setWidth(2))),
          margin: EdgeInsets.symmetric(vertical: ScreenUtil().setHeight(8)),
          child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: ScreenUtil().setWidth(10),
                vertical: ScreenUtil().setHeight(8)),
            child: Column(
              children: <Widget>[
                Column(
                  mainAxisSize: MainAxisSize.min,
                  children: _allProductsInvoiceWidget,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(flex: 4, child: BaseWidget.text('Thanh toán:')),
                    Expanded(
                      flex: 6,
                      child: TextFormField(
                        style: TextStyle(
                            color: Colors.black87,
                            fontSize: ScreenUtil().setSp(16),
                            fontFamily: 'QS',
                            fontWeight: FontWeight.w500),
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          hintText: '${Utils.formatMoney(1000000)}',
                          hintStyle: TextStyle(
                              color: Colors.grey[400],
                              fontSize: ScreenUtil().setSp(16),
                              fontFamily: 'QS',
                              fontWeight: FontWeight.w500),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.black87),
                          ),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.grey[400]),
                          ),
                        ),
                        cursorColor: Colors.black87,
                        onChanged: (value) => model.paid = double.parse(value),
                      ),
                    )
                  ],
                ),
                InkWell(
                  onTap: () => _setOnClickCreateNewProductElement(model),
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        color: Colors.blue),
                    margin: EdgeInsets.only(top: ScreenUtil().setHeight(20)),
                    padding: EdgeInsets.symmetric(
                        horizontal: ScreenUtil().setWidth(15),
                        vertical: ScreenUtil().setHeight(10)),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Icon(
                          Icons.add_circle_outline,
                          color: Colors.white,
                        ),
                        BaseWidget.text('\tThêm sản phẩm',
                            textColor: Colors.white,
                            fontWeight: FontWeight.w600),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }

//  void _addInvoice(int index, InvoiceViewModel model) {
//    model.allInvoicesMap[index] = new Invoice(
//        barcode: model.barcode,
//        productName: model.productName,
//        count: model.count,
//        wholesale: model.price);
//  }

  Padding _distributorComboBox(InvoiceViewModel model) {
    return Padding(
      padding: EdgeInsets.only(
          top: ScreenUtil().setHeight(10), bottom: ScreenUtil().setHeight(15)),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Icon(
              Icons.account_circle,
              color: Colors.black87,
            ),
          ),
          Expanded(
            flex: 9,
            child: DropdownButton(
              autofocus: true,
              isExpanded: true,
              isDense: true,
              icon: Icon(
                Icons.keyboard_arrow_down,
                color: Colors.black87,
              ),
              value: model.currentDistributor,
              items: model.distributorsDropDown
                  .map((value) => DropdownMenuItem(
                      value: value, child: BaseWidget.text(value)))
                  .toList(),
              onChanged: (value) => changedDropDownItem(value, model),
            ),
          )
        ],
      ),
    );
  }

  Widget _dateWidget(InvoiceViewModel model) {
//    debugPrint("AddNewInvoiceView - _dateWidget - date: ${model.invoiceDate}");
    return InkWell(
      onTap: () => _selectDatePicker(model),
      child: Padding(
        padding: EdgeInsets.only(
            top: ScreenUtil().setHeight(5), bottom: ScreenUtil().setHeight(10)),
        child: Row(
          children: <Widget>[
            Expanded(
                flex: 1,
                child: Icon(
                  Icons.date_range,
                  color: Colors.black87,
                )),
            Expanded(
              flex: 9,
              child: Center(
                child: BaseWidget.text(
                    '${DateFormat("dd/MM/yyyy").format(model.invoiceDate)}',
                    textColor: Colors.blue,
                    textSize: ScreenUtil().setSp(18),
                    fontWeight: FontWeight.w600),
              ),
            )
          ],
        ),
      ),
    );
  }

  void changedDropDownItem(String selectedDistributor, InvoiceViewModel model) {
    if (selectedDistributor.contains("Thêm đối tác")) {
      Navigator.push(
          context, CupertinoPageRoute(builder: (_) => AddNewDistributorView()));
    } else {
      model.currentDistributor = selectedDistributor;
    }
  }

  _selectDatePicker(InvoiceViewModel model) async {
    DateTime date = await showRoundedDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(DateTime.now().year - 10),
      lastDate: DateTime(DateTime.now().year + 10),
      borderRadius: 16,
      fontFamily: 'QS',
      imageHeader: AssetImage("assets/images/calendar_header.jpg"),
    );
    if (date != null) {
      model.invoiceDate = date;
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  _funcButton(InvoiceViewModel model) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.end,
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        InkWell(
          onTap: () => Navigator.pop(context),
          child: Container(
            width: ScreenUtil().setWidth(100),
            padding: EdgeInsets.symmetric(vertical: ScreenUtil().setHeight(10)),
            alignment: Alignment.center,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                color: Colors.red),
            child: BaseWidget.text('Huỷ',
                textColor: Colors.white, fontWeight: FontWeight.w600),
          ),
        ),
        InkWell(
          onTap: () => _setOnClickAddInvoiceButton(context, model),
          child: Container(
            width: ScreenUtil().setWidth(100),
            margin: EdgeInsets.only(left: ScreenUtil().setWidth(10)),
            padding: EdgeInsets.symmetric(vertical: ScreenUtil().setHeight(10)),
            alignment: Alignment.center,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                color: Colors.blue),
            child: BaseWidget.text('Hoàn tất',
                textColor: Colors.white, fontWeight: FontWeight.w600),
          ),
        ),
      ],
    );
  }

  _setOnClickAddInvoiceButton(
      BuildContext context, InvoiceViewModel model) async {
    if (model.allInvoicesMap == null ||
        model.allInvoicesMap.length == 0 ||
        model.allInvoicesMap.length < _allProductsInvoiceWidget.length) {
      Toast.show('Chưa điền thông tin hoá đơn', context,
          backgroundColor: Colors.red, textColor: Colors.white);
    } else {
      if (model.currentDistributor == "Thêm đối tác") {
        Toast.show("Bạn chưa chọn đối tác", context,
            backgroundColor: Colors.red, textColor: Colors.white);
      } else {
        await model.addNewInvoice();
        if (model.insertState == ViewState.Error) {
          Toast.show("Lỗi", context,
              backgroundColor: Colors.red, textColor: Colors.white);
        } else if (model.insertState == ViewState.Success) {
          Toast.show("Thành công", context,
              backgroundColor: Colors.blue, textColor: Colors.white);
          Navigator.pop(context);
        }

      }
    }
  }

  _setOnClickCreateNewProductElement(InvoiceViewModel model) {
    if (model.allInvoicesMap == null ||
        model.allInvoicesMap.length == 0 ||
        model.allInvoicesMap.length != _allProductsInvoiceWidget.length) {
      Toast.show('Chưa điền thông tin sản phẩm', context,
          backgroundColor: Colors.red, textColor: Colors.white);
    } else {
      int lastInvoiceIndex = model.allInvoicesMap.keys.toList().last;
      if (model.allInvoicesMap[lastInvoiceIndex] == null) {
        Toast.show('Chưa điền thông tin sản phẩm', context,
            backgroundColor: Colors.red, textColor: Colors.white);
      } else {
        Invoice invoice = model.allInvoicesMap[lastInvoiceIndex];
        if (invoice.productName.isEmpty) {
          Toast.show('Chưa điền tên sản phẩm', context,
              backgroundColor: Colors.red, textColor: Colors.white);
        } else if (invoice.count.isEmpty) {
          Toast.show('Chưa điền số lượng', context,
              backgroundColor: Colors.red, textColor: Colors.white);
        } else if (invoice.wholesale.isEmpty) {
          Toast.show('Chưa điền giá', context,
              backgroundColor: Colors.red, textColor: Colors.white);
        } else {
//          _addInvoice(_allProductsInvoiceWidget.length - 1, model);
          setState(() {
            _allProductsInvoiceWidget.add(ProductElementView(
              model: model,
              index: _allProductsInvoiceWidget.length,
            ));
          });
        }
      }
    }
  }
}
