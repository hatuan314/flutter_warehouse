import 'dart:convert';

import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:warehouse_of_pikata/models/invoice.dart';
import 'package:warehouse_of_pikata/scoped_models/invoices/invoice_view_model.dart';
import 'package:warehouse_of_pikata/ui/widget/base_widget.dart';

class ProductElementView extends StatefulWidget {
  final InvoiceViewModel model;
  final int index;

  const ProductElementView({Key key, this.model, this.index}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ProductElementViewState();
}

class _ProductElementViewState extends State<ProductElementView> {
  String _barcode;

  FocusNode _countFocus = FocusNode();
  FocusNode _priceFocus = FocusNode();

  final formKey = GlobalKey<FormState>();

  String currentText = "";

//  GlobalKey<AutoCompleteTextFieldState<String>> key = new GlobalKey();
  TextEditingController _nameController = TextEditingController();

  Future scanBarcode() async {
    try {
      String barcode = await BarcodeScanner.scan();
//      print('barcode: $barcode');
      setState(() {
        _barcode = barcode;
      });
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
        setState(() {
          _barcode = 'The user did not grant the camera permission!';
        });
      } else {
        setState(() {
          _barcode = 'Unknown error: $e';
        });
      }
    } on FormatException {
      setState(() {
        _barcode =
            'null (User returned using the "back"-button before scanning anything. Result)';
      });
    } catch (e) {
      setState(() {
        _barcode = 'Unknown error: $e';
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      _barcode = '';
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    ScreenUtil.init(context, width: 375, height: 812, allowFontScaling: true);
    return Form(
      key: formKey,
      child: Column(
        children: <Widget>[
          TypeAheadFormField(
            textFieldConfiguration: TextFieldConfiguration(
              decoration: InputDecoration(
                hintText: "Tên sản phẩm",
                hintStyle: TextStyle(
                    color: Colors.grey[400],
                    fontSize: ScreenUtil().setSp(16),
                    fontFamily: 'QS',
                    fontWeight: FontWeight.w500),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.black87),
                ),
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey[400]),
                ),
              ),
              onChanged: (value) => _onChangeText(value, 0),
              controller: this._nameController,
            ),
            suggestionsCallback: (pattern) {
              return _getSuggestions(pattern);
            },
            itemBuilder: (context, suggestion) {
              return ListTile(
                title: Text(suggestion),
              );
            },
            transitionBuilder: (context, suggestionsBox, controller) {
              return suggestionsBox;
            },
            onSuggestionSelected: (suggestion) {
              this._nameController.text = suggestion;
              debugPrint(
                  'ProductElementView - TypeAheadFormField - _nameController: ${_nameController.text}');
              _onChangeText(suggestion, 0);
              debugPrint(
                  'ProductElementView - TypeAheadFormField - this.widget.model.productName: ${this.widget.model.productName}');
            },
            onSaved: (value) => this.widget.model.productName = value,
          ),
          SizedBox(
            height: ScreenUtil().setHeight(10),
          ),
          Row(
            children: <Widget>[
              new RaisedButton(
                shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(5.0),
                ),
                color: Colors.grey[100],
                onPressed: scanBarcode,
                child: BaseWidget.text('Scan Barcode'),
              ),
              Expanded(child: BaseWidget.text('\t${_barcode}')),
            ],
          ),
          Padding(
            padding: EdgeInsets.only(bottom: ScreenUtil().setHeight(10)),
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: EdgeInsets.only(right: ScreenUtil().setWidth(5)),
                    child: _textForm(
                        'Số lượng', 1, this.widget.model, _countFocus,
                        nextFocus: _priceFocus),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: EdgeInsets.only(left: ScreenUtil().setWidth(5)),
                    child: _textForm(
                        'Giá nhập vào', 2, this.widget.model, _priceFocus),
                  ),
                ),
              ],
            ),
          ),
          Divider(
            color: Colors.grey[300],
            thickness: ScreenUtil().setHeight(0.75),
          )
        ],
      ),
    );
  }

  TextFormField _textForm(String hintText, int textFieldIndex,
      InvoiceViewModel invoiceViewModel, FocusNode currentFocus,
      {FocusNode nextFocus}) {
    return TextFormField(
      style: TextStyle(
          color: Colors.black87,
          fontSize: ScreenUtil().setSp(16),
          fontFamily: 'QS',
          fontWeight: FontWeight.w500),
      keyboardType:
          textFieldIndex == 0 ? TextInputType.text : TextInputType.phone,
      textInputAction: currentFocus == _priceFocus
          ? TextInputAction.done
          : TextInputAction.next,
      focusNode: currentFocus,
      onFieldSubmitted: currentFocus == _priceFocus
          ? (value) {
//        print('done');
              _priceFocus.unfocus();
            }
          : (term) {
              _fieldFocusChange(context, currentFocus, nextFocus);
            },
      decoration: InputDecoration(
        hintText: hintText,
        hintStyle: TextStyle(
            color: Colors.grey[400],
            fontSize: ScreenUtil().setSp(16),
            fontFamily: 'QS',
            fontWeight: FontWeight.w500),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.black87),
        ),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.grey[400]),
        ),
      ),
      cursorColor: Colors.black87,
      onChanged: (value) => _onChangeText(value, textFieldIndex),
    );
  }

  void _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
//    print('next');
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  _onChangeText(String value, int textFieldIndex) {
    {
      if (this.widget.model.allInvoicesMap[this.widget.index] != null) {
        this.widget.model.productName =
            this.widget.model.allInvoicesMap[this.widget.index].productName;
        this.widget.model.count =
            this.widget.model.allInvoicesMap[this.widget.index].count;
        this.widget.model.price =
            this.widget.model.allInvoicesMap[this.widget.index].wholesale;
      }
//      print('index: ${this.widget.index}');
      switch (textFieldIndex) {
        case 0:
          this.widget.model.productName = value;
//          debugPrint('ProductElementView - _onChangeText - productName: $value');

          break;
        case 1:
          this.widget.model.count = value;
          break;
        case 2:
          this.widget.model.price = value;
          break;
      }
//      print('length: ${this.widget.model.allInvoicesMap.length}');
      this.widget.model.allInvoicesMap[this.widget.index] = Invoice(
          barcode: _barcode,
          productName: this.widget.model.productName,
          count: this.widget.model.count,
          wholesale: this.widget.model.price);
      debugPrint(
          "ProductElementView - _onChangeText - invoice - ${this.widget.index}: ${json.encode(this.widget.model.allInvoicesMap[this.widget.index].toJson())} ");
    }
  }

  _getSuggestions(String query) {
    List<String> matches = List();
    matches.addAll(widget.model.allProdctsMap.keys.toList());

    matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));

    return matches;
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
}
