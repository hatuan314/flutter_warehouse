import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:toast/toast.dart';
import 'package:warehouse_of_pikata/enums/view_state.dart';
import 'package:warehouse_of_pikata/scoped_models/category/category_view_model.dart';
import 'package:warehouse_of_pikata/ui/view/base_view.dart';
import 'package:warehouse_of_pikata/ui/widget/base_widget.dart';

class AddNewCategoryView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _AddNewCategoryViewState();

}

class _AddNewCategoryViewState extends State<AddNewCategoryView> {
  GlobalKey<FormState> _textFormKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    ScreenUtil.init(context, width: 375, height: 812, allowFontScaling: true);
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [Colors.blue[900], Colors.red[900]],
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          tileMode: TileMode.clamp,
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: BaseWidget.appBar('Thêm danh mục'),
        body: _mBody(),
      ),
    );
  }

  _mBody() {
    return BaseView<CategoryViewModel>(
        builder: (context, child, model) {
          return Stack(
            alignment: Alignment.bottomCenter,
            children: <Widget>[
              SizedBox(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
              ),
              Form(
                key: _textFormKey,
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      borderRadius:
                      BorderRadius.vertical(top: Radius.circular(15)),
                      color: Colors.grey[100]),
                  padding: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(10),
                      vertical: ScreenUtil().setHeight(8)),
                  child: CustomScrollView(
                    shrinkWrap: true,
                    slivers: <Widget>[
                      SliverToBoxAdapter(
                        child: Padding(
                          padding: EdgeInsets.only(
                              left: ScreenUtil().setWidth(15),
                              top: ScreenUtil().setHeight(10),
                            bottom: ScreenUtil().setHeight(20)
                          ),
                          child: TextFormField(
                            style: TextStyle(
                                color: Colors.black87,
                                fontSize: ScreenUtil().setSp(16),
                                fontFamily: 'QS',
                                fontWeight: FontWeight.w500),
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                              hintText: 'Tên danh mục',
                              hintStyle: TextStyle(
                                  color: Colors.grey[400],
                                  fontSize: ScreenUtil().setSp(16),
                                  fontFamily: 'QS',
                                  fontWeight: FontWeight.w500),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.black87),
                              ),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey[400]),
                              ),
                            ),
                            cursorColor: Colors.black87,
                            onChanged: (value) {
//                              debugPrint("AdNewCategory - _mBody - onChange: $value");
                              model.name = value;
                            },
                          ),
                        ),
                      ),
                      SliverToBoxAdapter(
                        child: _funcButton(model),
                      )
                    ],
                  ),
                ),
              )
            ],
          );
        }
    );
  }

  _funcButton(CategoryViewModel model) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.end,
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        InkWell(
          onTap: () => Navigator.pop(context),
          child: Container(
            width: ScreenUtil().setWidth(100),
            padding: EdgeInsets.symmetric(vertical: ScreenUtil().setHeight(10)),
            alignment: Alignment.center,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                color: Colors.red),
            child: BaseWidget.text('Huỷ',
                textColor: Colors.white, fontWeight: FontWeight.w600),
          ),
        ),
        InkWell(
          onTap: () => _setOnClickAddButton(model),
          child: Container(
            width: ScreenUtil().setWidth(100),
            margin: EdgeInsets.only(left: ScreenUtil().setWidth(10)),
            padding: EdgeInsets.symmetric(vertical: ScreenUtil().setHeight(10)),
            alignment: Alignment.center,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                color: Colors.blue),
            child: BaseWidget.text('Hoàn tất',
                textColor: Colors.white, fontWeight: FontWeight.w600),
          ),
        ),
      ],
    );
  }

  _setOnClickAddButton(CategoryViewModel model) async {
    FocusScope.of(context).requestFocus(new FocusNode());
    if (_textFormKey.currentState.validate()) {
      await model.addNewCategory();
      if(model.addState == ViewState.Error) {
        Toast.show('Lỗi', context, textColor: Colors.white, backgroundColor: Colors.red);
      } else if(model.addState == ViewState.NotReady) {
        Toast.show('Lỗi', context, textColor: Colors.white, backgroundColor: Colors.red);
      } else if (model.addState == ViewState.Success) {
        Toast.show('Thêm thành công', context, textColor: Colors.white, backgroundColor: Colors.blue);
        Navigator.pop(context);
      }
    }
  }
}
