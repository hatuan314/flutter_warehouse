import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:toast/toast.dart';
import 'package:warehouse_of_pikata/enums/view_state.dart';
import 'package:warehouse_of_pikata/scoped_models/register_view_model.dart';
import 'package:warehouse_of_pikata/ui/screen/background_screen.dart';
import 'package:warehouse_of_pikata/ui/view/base_view.dart';
import 'package:warehouse_of_pikata/ui/view/home/home_view.dart';
import 'package:warehouse_of_pikata/ui/widget/base_widget.dart';

class SignInView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => SignInViewState();
}

class SignInViewState extends State<SignInView> {
  GlobalKey<FormState> _textFormKey = new GlobalKey();
  String _username = '';
  String _password = '';
  bool _isBiometricAuth = false;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    ScreenUtil.init(context, width: 375, height: 812, allowFontScaling: true);
    return Scaffold(
      body: BaseView<RegisterViewModel>(onModelReady: (model) {
        model.checkRegister();
        model.checkBiometrics().then((value) {
          setState(() {
            _isBiometricAuth = value;
            print('finger prinner: $_isBiometricAuth');
          });
        });
      }, builder: (context, child, model) {
        if (model.state == ViewState.Success) {
          return Container(
            child: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                FancyBackgroundApp(),
                Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Form(
                      key: _textFormKey,
                      child: Container(
                        width: MediaQuery.of(context).size.width * 4 / 5,
                        padding: EdgeInsets.symmetric(
                            horizontal: ScreenUtil().setWidth(15),
                            vertical: ScreenUtil().setHeight(20)),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(15)),
                            color: Colors.white54),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            BaseWidget.text(
                                model.isRegister ? 'Đăng nhập' : 'Đăng ký',
                                textSize: ScreenUtil().setSp(28),
                                fontWeight: FontWeight.w600),
                            Padding(
                              padding: EdgeInsets.only(
                                  top: ScreenUtil().setHeight(40)),
                              child: _textFormElementWidget('Tên đăng nhập',
                                  Icons.account_circle, 1, model),
                            ),
                            Stack(
                              alignment: Alignment.centerRight,
                              children: <Widget>[
                                _textFormElementWidget(
                                    'Mật khẩu', Icons.lock, 2, model),
                                InkWell(
                                  onTap: () =>
                                      model.showPassword = !model.showPassword,
                                  child: Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: ScreenUtil().setWidth(5)),
                                    child: Icon(
                                      model.showPassword
                                          ? Icons.visibility
                                          : Icons.visibility_off,
                                      color: Colors.grey,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            InkWell(
                              onTap: () => _setOnClickSignInButton(model),
                              child: Container(
                                margin: EdgeInsets.only(
                                    top: ScreenUtil().setHeight(20)),
                                padding: EdgeInsets.symmetric(
                                    horizontal: ScreenUtil().setWidth(40),
                                    vertical: ScreenUtil().setHeight(8)),
                                decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(25)),
                                  gradient: LinearGradient(
                                    colors: [
                                      Colors.lightBlueAccent[100],
                                      Colors.blue[800]
                                    ],
                                    begin: Alignment.centerLeft,
                                    end: Alignment.centerRight,
                                    tileMode: TileMode.clamp,
                                  ),
                                ),
                                child: BaseWidget.text(
                                    model.isRegister ? 'ĐĂNG NHẬP' : "ĐĂNG KÝ",
                                    textColor: Colors.white,
                                    fontWeight: FontWeight.w600),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(
                        margin:
                            EdgeInsets.only(top: ScreenUtil().setHeight(25)),
                        child: InkWell(
                          onTap: () => _scanFinger(model),
                          child: Icon(
                            Icons.fingerprint,
                            size: ScreenUtil().setSp(50),
                            color: Colors.white,
                          ),
                        ))
                  ],
                )
              ],
            ),
          );
        } else {
          return Container(
            color: Colors.transparent,
          );
        }
      }),
    );
  }

  Container _textFormElementWidget(
      String title, IconData icon, int textIndex, RegisterViewModel model) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: ScreenUtil().setHeight(5)),
      padding: EdgeInsets.only(right: ScreenUtil().setWidth(10)),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(15)),
          color: Colors.white),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 15,
            child: Icon(
              icon,
              color: Colors.black87,
            ),
          ),
          Expanded(
            flex: 85,
            child: TextFormField(
              obscureText: textIndex == 2 && model.showPassword == false,
              style: TextStyle(
                  fontFamily: 'QS',
                  fontSize: ScreenUtil().setSp(16),
                  fontWeight: FontWeight.w400),
              decoration: InputDecoration(
                  hintText: '$title',
                  hintStyle: TextStyle(
                      fontFamily: 'QS',
                      fontSize: ScreenUtil().setSp(16),
                      fontWeight: FontWeight.w400),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.transparent),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.transparent),
                  )),
              validator: (value) => model.validateRequiredField(value),
              onChanged: (value) => textIndex == 1
                  ? model.username = value
                  : model.password = value,
            ),
          )
        ],
      ),
    );
  }

  _setOnClickSignInButton(RegisterViewModel model) async {
    FocusScope.of(context).requestFocus(new FocusNode());
    if (_textFormKey.currentState.validate()) {
      bool flag = false;
      if (model.isRegister) {
        flag = await model.signIn();
      } else {
        flag = await model.signUp();
      }

      switch (model.signInState) {
        case ViewState.Success:
          print('Success');
          Toast.show("Thành công", context,
              backgroundColor: Colors.blue, textColor: Colors.white);
          Navigator.pushReplacement(
              context, CupertinoPageRoute(builder: (_) => HomeView()));
          break;
        case ViewState.Busy:
          break;
        default:
          Toast.show(
            "Lỗi",
            context,
            backgroundColor: Colors.redAccent,
            textColor: Colors.white,
          );
          break;
      }
    }
  }

  _scanFinger(RegisterViewModel model) {
    if (model.isRegister == false) {
      Toast.show('Bạn phải đăng ký trước', context,
          duration: Toast.LENGTH_LONG);
    } else {
      if (_isBiometricAuth == false) {
        Toast.show('Thiết bị của bạn không hỗ trợ tính năng này', context,
            duration: Toast.LENGTH_LONG);
      } else {
        model.biometricAuthenticate().then((value) {
          if (value) {
            Toast.show('Success', context,
                duration: Toast.LENGTH_SHORT, backgroundColor: Colors.blue);
            Navigator.pushReplacement(
                context, CupertinoPageRoute(builder: (_) => HomeView()));
          } else {
            Toast.show('Từ chỗi truy cập', context,
                duration: Toast.LENGTH_SHORT);
          }
        });
      }
    }
  }
}
