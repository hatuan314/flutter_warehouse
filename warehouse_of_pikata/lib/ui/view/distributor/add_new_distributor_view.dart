import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:toast/toast.dart';
import 'package:warehouse_of_pikata/enums/view_state.dart';
import 'package:warehouse_of_pikata/scoped_models/distributor_view_model.dart';
import 'package:warehouse_of_pikata/ui/view/base_view.dart';
import 'package:warehouse_of_pikata/ui/widget/base_widget.dart';
import 'package:warehouse_of_pikata/utils.dart';

class AddNewDistributorView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _AddNewDistributorViewState();
}

class _AddNewDistributorViewState extends State<AddNewDistributorView> {
  GlobalKey<FormState> _textFormKey = new GlobalKey();

  FocusNode _nameFocus = new FocusNode();
  FocusNode _addressFocus = new FocusNode();
  FocusNode _phoneFocus1 = new FocusNode();
  FocusNode _phoneFocus2 = new FocusNode();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    ScreenUtil.init(context, width: 375, height: 812, allowFontScaling: true);
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [Colors.blue[900], Colors.red[900]],
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          tileMode: TileMode.clamp,
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: BaseWidget.appBar('Thêm đối tác'),
        body: BaseView<DistributorViewModel>(builder: (context, child, model) {
          return Form(
            key: _textFormKey,
            child: Stack(
              alignment: Alignment.bottomCenter,
              children: <Widget>[
                SizedBox(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                ),
                Container(
                  decoration: BoxDecoration(
                    borderRadius:
                        BorderRadius.vertical(top: Radius.circular(15)),
                    color: Colors.grey[100],
                  ),
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        _textFormElement(model, Icons.account_circle,
                            'Tên đối tác', _nameFocus, 0,
                            nextNode: _addressFocus),
                        Padding(
                          padding: EdgeInsets.only(
                              left: ScreenUtil().setWidth(15),
                              top: ScreenUtil().setHeight(10)),
                          child: Row(
                            children: <Widget>[
                              BaseWidget.text('Màu đại diện:\t'),
                              IconButton(
                                icon: Icon(
                                  Icons.fiber_manual_record,
                                  color: model.distributorColor,
                                ),
                                onPressed: () =>
                                    _choiceColorDialog(context, model),
                              )
                            ],
                          ),
                        ),
                        _textFormElement(
                            model, Icons.home, 'Địa chỉ', _addressFocus, 1,
                            nextNode: _phoneFocus1),
                        _textFormElement(model, Icons.phone, 'Số điện thoại 1',
                            _phoneFocus1, 2,
                            nextNode: _phoneFocus2),
                        _textFormElement(model, Icons.phone, 'Số điện thoại 2',
                            _phoneFocus2, 3),
                        InkWell(
                          onTap: () => _setOnClickAddButton(model),
                          child: Container(
                            width: ScreenUtil().setWidth(100),
                            margin: EdgeInsets.only(
                                left: ScreenUtil().setWidth(10),
                                top: ScreenUtil().setHeight(20),
                                right: ScreenUtil().setWidth(10),
                                bottom: ScreenUtil().setHeight(10)),
                            padding: EdgeInsets.symmetric(
                                vertical: ScreenUtil().setHeight(10)),
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(5)),
                                color: Colors.blue),
                            child: BaseWidget.text('Thêm',
                                textColor: Colors.white,
                                fontWeight: FontWeight.w600),
                          ),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          );
        }),
      ),
    );
  }

  Row _textFormElement(DistributorViewModel model, IconData icon,
      String hintText, FocusNode presentNode, int textFieldIndex,
      {FocusNode nextNode}) {
    return Row(
      children: <Widget>[
        Expanded(flex: 15, child: Icon(icon, color: Colors.black87)),
        Expanded(
          flex: 85,
          child: Padding(
            padding: EdgeInsets.only(right: ScreenUtil().setWidth(15)),
            child: TextFormField(
              style: TextStyle(
                  fontFamily: 'QS',
                  fontSize: ScreenUtil().setSp(16),
                  fontWeight: FontWeight.w600),
              keyboardType: textFieldIndex == 2 || textFieldIndex == 3
                  ? TextInputType.phone
                  : TextInputType.text,
              textInputAction: presentNode == _phoneFocus2
                  ? TextInputAction.done
                  : TextInputAction.next,
              focusNode: presentNode,
              onFieldSubmitted: presentNode == _phoneFocus2
                  ? (value) {
                      _phoneFocus2.unfocus();
                    }
                  : (term) {
                      _fieldFocusChange(context, presentNode, nextNode);
                    },
              decoration: InputDecoration(
                hintText: hintText,
                hintStyle: TextStyle(
                    fontFamily: 'QS',
                    fontSize: ScreenUtil().setSp(16),
                    color: Colors.grey[400]),
                errorStyle: TextStyle(
                    fontFamily: 'QS',
                    fontSize: ScreenUtil().setSp(14),
                    color: Colors.red),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.black87),
                ),
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey[400]),
                ),
              ),
              validator: (value) {
                if ((value == null || value.isEmpty) ) {
                  if (textFieldIndex != 3)
                    return 'Trường này không được để trống';
                  else
                    model.phone2 = "";
                    return null;
                }
                return null;
              },
              cursorColor: Colors.black87,
              onChanged: (value) {
                switch (textFieldIndex) {
                  case 0:
                    model.name = value;
                    break;
                  case 1:
                    model.address = value;
                    break;
                  case 2:
                    model.phone1 = value;
                    break;
                  case 3:
                    debugPrint(
                        'AddNewDistributorView - TextFormFeild - changeValue: $value');
                    model.phone2 = value;
                    break;
                }
              },
            ),
          ),
        ),
      ],
    );
  }

  void _fieldFocusChange(
      BuildContext context, FocusNode presentNode, FocusNode nextNode) {
    presentNode.unfocus();
    FocusScope.of(context).requestFocus(nextNode);
  }

  _choiceColorDialog(BuildContext context, DistributorViewModel model) {
    showDialog(
        context: context,
        builder: (BuildContext context) => AlertDialog(
              backgroundColor: Colors.white,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              title: Center(
                child: BaseWidget.text('Chọn màu đại diện',
                    fontWeight: FontWeight.w700,
                    textSize: ScreenUtil().setSp(18)),
              ),
              content: Container(
                height: MediaQuery.of(context).size.width * 2 / 3,
                width: MediaQuery.of(context).size.width * 2 / 3,
                color: Colors.white,
                child: Container(
                  width: MediaQuery.of(context).size.width * 2 / 3,
                  child: GridView.count(
                    shrinkWrap: true,
                    scrollDirection: Axis.vertical,
                    crossAxisCount: 4,
                    childAspectRatio: 1.0,
                    padding: const EdgeInsets.all(4.0),
                    mainAxisSpacing: 4.0,
                    crossAxisSpacing: 4.0,
                    children: Utils.distributorColors.keys
                        .map((k) => IconButton(
                              icon: Icon(
                                Icons.fiber_manual_record,
                                color: Utils.distributorColors[k],
                                size: ScreenUtil().setSp(50),
                              ),
                              onPressed: () {
                                model.color = k;
                                model.distributorColor =
                                    Utils.distributorColors[k];
                                Navigator.of(context).pop();
                              },
                            ))
                        .toList(),
                  ),
                ),
              ),
            ));
  }

  _setOnClickAddButton(DistributorViewModel model) async {
    FocusScope.of(context).requestFocus(new FocusNode());
    if (_textFormKey.currentState.validate()) {
      await model.addNewDistributor();
    debugPrint('AddNewDistributorView - TextFormFeild - phone2: ${model.phone2}');
      if (model.createState == ViewState.Success) {
        Toast.show('Lưu thành công', context,
            backgroundColor: Colors.blue, textColor: Colors.white);
        Navigator.pop(context);
      } else if (model.createState == ViewState.NotReady) {
        Toast.show('Chưa thể lưu', context);
      } else if (model.createState == ViewState.Error) {
        Toast.show('Lỗi', context,
            backgroundColor: Colors.red, textColor: Colors.white);
      }
    }
  }
}
