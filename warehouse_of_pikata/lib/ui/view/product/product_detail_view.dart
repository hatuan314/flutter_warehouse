import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:toast/toast.dart';
import 'package:warehouse_of_pikata/enums/view_state.dart';
import 'package:warehouse_of_pikata/models/product.dart';
import 'package:warehouse_of_pikata/scoped_models/products/product_view_model.dart';
import 'package:warehouse_of_pikata/ui/view/base_view.dart';
import 'package:warehouse_of_pikata/ui/view/category/add_new_category.dart';
import 'package:warehouse_of_pikata/ui/widget/base_widget.dart';
import 'package:warehouse_of_pikata/ui/widget/loading/loading_view.dart';
import 'package:warehouse_of_pikata/utils.dart';

class ProductDetailView extends StatefulWidget {
  String name;
  String categoryId;

  ProductDetailView(this.name, this.categoryId);

  @override
  State<StatefulWidget> createState() => _ProductDetailViewState();
}

class _ProductDetailViewState extends State<ProductDetailView> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    ScreenUtil.init(context, width: 375, height: 812, allowFontScaling: true);
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [Colors.blue[900], Colors.red[900]],
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          tileMode: TileMode.clamp,
        ),
      ),
      child: BaseView<ProductViewModel>(onModelReady: (model) {
        model.getProductDetail(this.widget.name, this.widget.categoryId);
        model.getAllCategories(this.widget.categoryId);
      }, builder: (context, child, model) {
        switch (model.state) {
          case ViewState.NoDataAvailable:
            return Scaffold(
              backgroundColor: Colors.transparent,
              appBar: BaseWidget.appBar('${this.widget.name}'),
              body: Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    BaseWidget.text('Không có thông tin',
                        textColor: Colors.white54,
                        textSize: ScreenUtil().setSp(18)),
                  ],
                ),
              ),
            );
          case ViewState.Busy:
            return Scaffold(
              backgroundColor: Colors.transparent,
              appBar: BaseWidget.appBar('${this.widget.name}'),
              body: Center(
                child: LoadingView(),
              ),
            );
          case ViewState.Success:
            return Scaffold(
              backgroundColor: Colors.transparent,
              appBar: _mAppBar(model),
              body: Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                decoration: BoxDecoration(
                    borderRadius:
                    BorderRadius.only(topRight: Radius.circular(30)),
                    color: Colors.grey[100]),
                padding: EdgeInsets.symmetric(
                    horizontal: ScreenUtil().setWidth(12),
                    vertical: ScreenUtil().setHeight(12)),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      BaseWidget.text('${this.widget.name}',
                          textSize: ScreenUtil().setSp(18),
                          fontWeight: FontWeight.w700),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          BaseWidget.text('Danh mục:',
                              textSize: ScreenUtil().setSp(16),
                              fontWeight: FontWeight.w500),
                          Expanded(
                            child: Padding(
                              padding: EdgeInsets.only(
                                  top: ScreenUtil().setHeight(25),
                                  bottom: ScreenUtil().setHeight(18)),
                              child: DropdownButton(
                                autofocus: true,
                                isExpanded: true,
                                isDense: true,
                                icon: Icon(
                                  Icons.keyboard_arrow_down,
                                  color: Colors.black87,
                                ),
                                value: model.currentCategory,
                                items: model.allCategoriesMap.keys
                                    .toList()
                                    .map((categoryName) => DropdownMenuItem(
                                    value: categoryName,
                                    child: Center(
                                        child: BaseWidget.text(
                                            categoryName))))
                                    .toList(),
                                onChanged: (value) =>
                                    changedDropDownItem(context, value, model),
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: ScreenUtil().setHeight(8),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          RichText(
                            text: TextSpan(
                                text: 'Giá bán: ',
                                style: TextStyle(
                                  fontSize: ScreenUtil().setSp(16),
                                  color: Colors.black54,
                                  fontFamily: 'QS',
                                  fontWeight: FontWeight.w500,
                                ),
                                children: [
                                  TextSpan(
                                    text: model.retail.isEmpty
                                        ? '0 đ'
                                        : '${Utils.formatMoney(double.parse(model.retail))} đ',
                                    style: TextStyle(
                                      fontSize: ScreenUtil().setSp(16),
                                      color: Colors.red,
                                      fontFamily: 'QS',
                                      fontWeight: FontWeight.w700,
                                    ),
                                  )
                                ]),
                          ),
                          IconButton(
                            onPressed: () => _editRetailDialog(model),
                            icon: Icon(Icons.edit),
                          )
                        ],
                      ),
                      SizedBox(
                        height: ScreenUtil().setHeight(8),
                      ),
                      BaseWidget.text('Nhà phân phối:',
                          fontWeight: FontWeight.w600),
                      Divider(
                        color: Colors.grey[350],
                        thickness: 1.2,
                      ),
                      _allDistributorsListView(model)
                    ],
                  ),
                ),
              ),
            );
          default:
            return Scaffold(
              backgroundColor: Colors.transparent,
              appBar: BaseWidget.appBar('${this.widget.name}'),
              body: Center(
                child: BaseWidget.text('Lỗi',
                    textColor: Colors.white, textSize: ScreenUtil().setSp(18)),
              ),
            );
        }
      }),
    );
  }

  ListView _allDistributorsListView(ProductViewModel model) {
    return ListView.builder(
        shrinkWrap: true,
        itemCount: model.allProductsSameName.length,
        itemBuilder: (BuildContext context, index) {
          Product productDetail = model.allProductsSameName[index];
          return Card(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10))),
            child: Padding(
              padding: EdgeInsets.symmetric(
                  vertical: ScreenUtil().setHeight(10),
                  horizontal: ScreenUtil().setWidth(8)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(50)),
                        color: Utils
                            .distributorColors[productDetail.representColor]),
                    padding: EdgeInsets.symmetric(
                        horizontal: ScreenUtil().setWidth(8),
                        vertical: ScreenUtil().setHeight(5)),
                    child: BaseWidget.text('${productDetail.distributorName}',
                        textSize: ScreenUtil().setSp(13),
                        textColor: Colors.white),
                  ),
                  SizedBox(
                    height: ScreenUtil().setHeight(12),
                  ),
                  SizedBox(
                    height: ScreenUtil().setHeight(5),
                  ),
                  RichText(
                    text: TextSpan(
                        text: 'Giá nhập: ',
                        style: TextStyle(
                          fontSize: ScreenUtil().setSp(16),
                          color: Colors.black54,
                          fontFamily: 'QS',
                          fontWeight: FontWeight.w500,
                        ),
                        children: [
                          TextSpan(
                            text:
                                '${Utils.formatMoney(double.parse(productDetail.wholesale))} đ',
                            style: TextStyle(
                              fontSize: ScreenUtil().setSp(16),
                              color: Colors.blue,
                              fontFamily: 'QS',
                              fontWeight: FontWeight.w700,
                            ),
                          )
                        ]),
                  ),
                ],
              ),
            ),
          );
        });
  }

  void changedDropDownItem(
      BuildContext context, String selectedCategory, ProductViewModel model) {
    if (selectedCategory.contains("Thêm danh mục")) {
      Navigator.push(
          context, CupertinoPageRoute(builder: (_) => AddNewCategoryView()));
    } else {
      model.currentCategory = selectedCategory;
    }
  }

  _mAppBar(ProductViewModel model) {
    return AppBar(
      title: BaseWidget.text('Chi tiết',
          textSize: ScreenUtil().setSp(20),
          textColor: Colors.white,
          fontWeight: FontWeight.w600),
      centerTitle: true,
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      actions: <Widget>[
        InkWell(
          onTap: () => _setOnClickUpdateButton(model),
          child: Center(
              child: Padding(
            padding:
                EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(10)),
            child: BaseWidget.text("Cập nhật", textColor: Colors.white),
          )),
        )
      ],
    );
  }

  _editRetailDialog(ProductViewModel model) {
    return showDialog(
        context: context,
        builder: (BuildContext context) => Dialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              elevation: 0.0,
              backgroundColor: Colors.transparent,
              child: Container(
                decoration: new BoxDecoration(
                  color: Colors.white,
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.circular(20),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black26,
                      blurRadius: 10.0,
                      offset: const Offset(0.0, 10.0),
                    ),
                  ],
                ),
                child: Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(20),
                      vertical: ScreenUtil().setHeight(15)),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      BaseWidget.text("Giá bán",
                          fontWeight: FontWeight.w700,
                          textSize: ScreenUtil().setSp(18)),
                      Form(
                          key: _formKey,
                          child: TextFormField(
                            style: TextStyle(
                                color: Colors.black87,
                                fontSize: ScreenUtil().setSp(16),
                                fontFamily: 'QS',
                                fontWeight: FontWeight.w500),
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                              hintText: '${Utils.formatMoney(1000000)}',
                              hintStyle: TextStyle(
                                  color: Colors.grey[400],
                                  fontSize: ScreenUtil().setSp(16),
                                  fontFamily: 'QS',
                                  fontWeight: FontWeight.w500),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.black87),
                              ),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey[400]),
                              ),
                            ),
                            cursorColor: Colors.black87,
                            onChanged: (value) => model.retail = value,
                          )),
                      SizedBox(
                        height: ScreenUtil().setHeight(10),
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          RaisedButton(
                            onPressed: () => Navigator.of(context).pop(),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20),
                            ),
                            color: Colors.blue,
                            textColor: Colors.white,
                            child: SizedBox(
                              width: ScreenUtil().setWidth(80),
                              child: Center(
                                child: BaseWidget.text("Xác nhận",
                                    textColor: Colors.white),
                              ),
                            ),
                          ),
                          RaisedButton(
                            onPressed: () {
                              model.retail = '0';
                              Navigator.of(context).pop();
                            },
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20),
                            ),
                            color: Colors.red,
                            textColor: Colors.white,
                            child: SizedBox(
                              width: ScreenUtil().setWidth(80),
                              child: Center(
                                child: BaseWidget.text("Huỷ",
                                    textColor: Colors.white),
                              ),
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ));
  }

  _setOnClickUpdateButton(ProductViewModel model) async {
    await model.updateProduct();
    if(model.updateState == ViewState.Success) {
      Toast.show('Cập nhật thành công', context, backgroundColor: Colors.blue, textColor: Colors.white);
      Navigator.pop(context);
    } else if (model.updateState == ViewState.Busy){
      // Nothing here
    } else {
      Toast.show('Lỗi', context, backgroundColor: Colors.red, textColor: Colors.white);
    }
  }
}
