import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:warehouse_of_pikata/enums/view_state.dart';
import 'package:warehouse_of_pikata/models/category.dart';
import 'package:warehouse_of_pikata/models/product.dart';
import 'package:warehouse_of_pikata/scoped_models/products/product_view_model.dart';
import 'package:warehouse_of_pikata/ui/view/base_view.dart';
import 'package:warehouse_of_pikata/ui/view/invoice/add_new_invoice_view.dart';
import 'package:warehouse_of_pikata/ui/view/product/product_detail_view.dart';
import 'package:warehouse_of_pikata/ui/widget/base_widget.dart';
import 'package:warehouse_of_pikata/ui/widget/loading/loading_view.dart';
import 'package:warehouse_of_pikata/utils.dart';

class AllProductsOfTypeView extends StatelessWidget {
  Category category;

  AllProductsOfTypeView(this.category);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    ScreenUtil.init(context, width: 375, height: 812, allowFontScaling: true);
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [Colors.blue[900], Colors.red[900]],
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          tileMode: TileMode.clamp,
        ),
      ),
      child: BaseView<ProductViewModel>(onModelReady: (model) {
        model.getAllProducts(category.id);
      }, builder: (context, child, model) {
        switch (model.getProductState) {
          case ViewState.NoDataAvailable:
            return Scaffold(
              backgroundColor: Colors.transparent,
              appBar: BaseWidget.appBar('Hoá đơn'),
              body: Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    BaseWidget.text('Chưa có sản phẩm nào',
                        textColor: Colors.white54,
                        textSize: ScreenUtil().setSp(18)),
                    RaisedButton(
                      onPressed: () => Navigator.push(
                          context,
                          CupertinoPageRoute(
                              builder: (_) => AddNewInvoiceView())),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(5))),
                      color: Colors.blue,
                      child: BaseWidget.text("Thêm",
                          textSize: ScreenUtil().setSp(18),
                          textColor: Colors.white),
                    )
                  ],
                ),
              ),
            );
          case ViewState.Busy:
            return Scaffold(
              backgroundColor: Colors.transparent,
              appBar: BaseWidget.appBar('Hoá đơn'),
              body: Center(
                child: LoadingView(),
              ),
            );
          case ViewState.Success:
            return Scaffold(
              backgroundColor: Colors.transparent,
              appBar: mAppBar(context, model),
              body: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(30)),
                  color: Colors.grey[100],
                ),
                padding: EdgeInsets.only(top: ScreenUtil().setHeight(15)),
                child: model.changeView
                    ? _allProductGridView(context, model)
                    : _allProductListView(model),
              ),
            );
          default:
            return Scaffold(
              backgroundColor: Colors.transparent,
              appBar: BaseWidget.appBar('Hoá đơn'),
              body: Center(
                child: BaseWidget.text('Lỗi',
                    textColor: Colors.white, textSize: ScreenUtil().setSp(18)),
              ),
            );
        }
      }),
    );
  }

  _allProductListView(ProductViewModel model) {
    debugPrint("AllProductOfTypeView - _allProductListView - model.allProductCountsMap.length: ${model.allProductsMap.length}");
    return ListView.builder(
        itemCount: model.allProductsMap.length,
        itemBuilder: (BuildContext context, index) {
          String productName = model.allProductsMap.keys.toList()[index];
          return _productElementWidget(context, model, productName);
        });
  }

  InkWell _productElementWidget(
      BuildContext context, ProductViewModel model, String productName) {
    Product product = model.allProductsMap[productName][0];
    return InkWell(
      onTap: () => Navigator.push(
          context,
          CupertinoPageRoute(
              builder: (_) => ProductDetailView(product.name, product.categoryId))),
      child: Card(
        elevation: 3,
        color: Colors.white,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10))),
        margin: EdgeInsets.symmetric(
            horizontal: ScreenUtil().setWidth(15),
            vertical: ScreenUtil().setHeight(8)),
        child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: ScreenUtil().setWidth(8),
              vertical: ScreenUtil().setHeight(8)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              BaseWidget.text('$productName', fontWeight: FontWeight.w700),
              SizedBox(
                height: ScreenUtil().setHeight(18),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  BaseWidget.text('Giá bán:', textSize: ScreenUtil().setSp(14)),
                  BaseWidget.text(product.retail != "null"
                      ? '\t${Utils.formatMoney(double.parse(product.retail))} đ'
                      : "0 đ", textSize: ScreenUtil().setSp(16), textColor: Colors.red, fontWeight: FontWeight.bold),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  _allProductGridView(BuildContext context, ProductViewModel model) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(8)),
      child: GridView.builder(
          itemCount: model.allProductsMap.length,
          gridDelegate:
              new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
          itemBuilder: (BuildContext context, int index) {
            String productName = model.allProductsMap.keys.toList()[index];
            return _productGridElementWidget(context, model, productName);
          }),
    );
  }

  Widget _productGridElementWidget(
      BuildContext context, ProductViewModel model, String productName) {
    Product product = model.allProductsMap[productName][0];
    return InkWell(
      onTap: () => Navigator.push(
          context,
          CupertinoPageRoute(
              builder: (_) => ProductDetailView(product.name, product.categoryId))),
      child: Card(
        elevation: 3,
        color: Colors.white,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(8))),
        margin: EdgeInsets.symmetric(
            horizontal: ScreenUtil().setWidth(5),
            vertical: ScreenUtil().setHeight(8)),
        child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: ScreenUtil().setWidth(8),
              vertical: ScreenUtil().setHeight(8)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                '$productName',
                style: TextStyle(
                    fontSize: ScreenUtil().setSp(16),
                    color: Colors.black87,
                    fontFamily: 'QS',
                    fontWeight: FontWeight.w700),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
              SizedBox(
                height: ScreenUtil().setHeight(5),
              ),
              RichText(
                text: TextSpan(
                    text: 'Giá bán:',
                    style: TextStyle(
                        color: Colors.black87,
                        fontSize: ScreenUtil().setSp(14),
                        fontFamily: 'QS',
                        fontWeight: FontWeight.w500),
                    children: [
                      TextSpan(
                        text: product.retail != "null"
                            ? '\t${Utils.formatMoney(double.parse(product.retail))} đ'
                            : "0 đ",
                        style: TextStyle(
                            color: Colors.red,
                            fontFamily: 'QS',
                            fontSize: ScreenUtil().setSp(14),
                            fontWeight: FontWeight.w700),
                      )
                    ]),
              ),
            ],
          ),
        ),
      ),
    );
  }

  mAppBar(BuildContext context, ProductViewModel model) {
    return AppBar(
      title: BaseWidget.text('${category.name}',
          textSize: ScreenUtil().setSp(20),
          textColor: Colors.white,
          fontWeight: FontWeight.w600),
      centerTitle: true,
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      actions: <Widget>[
        IconButton(
          icon: Icon(
            model.changeView ? Icons.dashboard : Icons.view_list,
            color: Colors.white,
          ),
          onPressed: () => model.changeView = !model.changeView,
        ),
        IconButton(
          icon: Icon(Icons.search, color: Colors.white),
        )
      ],
    );
  }
}
