import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:warehouse_of_pikata/enums/view_state.dart';
import 'package:warehouse_of_pikata/models/invoice.dart';
import 'package:warehouse_of_pikata/scoped_models/invoices/invoice_view_model.dart';
import 'package:warehouse_of_pikata/ui/view/base_view.dart';
import 'package:warehouse_of_pikata/ui/view/invoice/add_new_invoice_view.dart';
import 'package:warehouse_of_pikata/ui/widget/base_widget.dart';
import 'package:warehouse_of_pikata/ui/widget/loading/loading_view.dart';
import 'package:warehouse_of_pikata/utils.dart';

class InvoiceTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    ScreenUtil.init(context, width: 375, height: 812, allowFontScaling: true);
    return BaseView<InvoiceViewModel>(onModelReady: (model) {
      model.getAllInvoices();
    }, builder: (context, child, model) {
      switch (model.getInvoiceState) {
        case ViewState.NoDataAvailable:
          return Scaffold(
            backgroundColor: Colors.transparent,
            appBar: BaseWidget.appBar('Hoá đơn'),
            body: Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  BaseWidget.text('Chưa có hoá đơn nào',
                      textColor: Colors.white54,
                      textSize: ScreenUtil().setSp(18)),
                  RaisedButton(
                    onPressed: () => Navigator.push(
                        context,
                        CupertinoPageRoute(
                            builder: (_) => AddNewInvoiceView())),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(5))),
                    color: Colors.blue,
                    child: BaseWidget.text("Thêm",
                        textSize: ScreenUtil().setSp(18),
                        textColor: Colors.white),
                  )
                ],
              ),
            ),
          );
        case ViewState.Busy:
          return Scaffold(
            backgroundColor: Colors.transparent,
            appBar: BaseWidget.appBar('Hoá đơn'),
            body: Center(
              child: LoadingView(),
            ),
          );
        case ViewState.Success:
          List<int> allInvoiceDates = model.allInvoiceByDayMap.keys.toList();
          return Scaffold(
            backgroundColor: Colors.transparent,
            appBar: BaseWidget.appBar('Hoá đơn'),
            body: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.vertical(top: Radius.circular(20)),
                  color: Colors.grey[100]),
              padding: EdgeInsets.only(top: ScreenUtil().setHeight(10)),
              child: ListView.builder(
                  itemCount: allInvoiceDates.length,
                  itemBuilder: (context, index) {
                    int date = allInvoiceDates[index];
                    List<Invoice> invoices = model.allInvoiceByDayMap[date];
                    return _invoiceElementWidget(model, invoices, date);
                  }),
            ),
          );
        default:
          return Scaffold(
            backgroundColor: Colors.transparent,
            appBar: BaseWidget.appBar('Hoá đơn'),
            body: Center(
              child: BaseWidget.text('Lỗi',
                  textColor: Colors.white, textSize: ScreenUtil().setSp(18)),
            ),
          );
      }
    });
  }

  Card _invoiceElementWidget(
      InvoiceViewModel model, List<Invoice> invoices, int date) {
//    debugPrint('InvoiceTab - _invoiceElementWidget - invoices: ${invoices.length}');
    return Card(
      elevation: 3,
      color: Colors.white,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8))),
      margin: EdgeInsets.symmetric(
          horizontal: ScreenUtil().setWidth(10),
          vertical: ScreenUtil().setHeight(8)),
      child: Padding(
        padding: EdgeInsets.symmetric(
            horizontal: ScreenUtil().setWidth(8),
            vertical: ScreenUtil().setHeight(8)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                  color: Utils.distributorColors[invoices[0].representColor]),
              padding: EdgeInsets.symmetric(
                  horizontal: ScreenUtil().setWidth(8),
                  vertical: ScreenUtil().setHeight(5)),
              child: BaseWidget.text('${invoices[0].distributorName}',
                  textColor: Colors.white),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: ScreenUtil().setWidth(5),
                  vertical: ScreenUtil().setHeight(3)),
              child: BaseWidget.text(
                  '${DateFormat.yMd().format(DateTime.fromMillisecondsSinceEpoch((int.parse(invoices[0].date))))}',
                  textColor: Colors.grey[400],
                  textSize: ScreenUtil().setSp(11)),
            ),
            Divider(
              color: Colors.grey[350],
              height: 10,
              thickness: 0.5,
            ),
            ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: invoices.length,
                itemBuilder: (context, index) {
                  Invoice invoice = invoices[index];
                  return _productInvoiceElementWidget(invoice);
                }),
            Divider(
              color: Colors.grey[350],
              height: 10,
              thickness: 0.5,
            ),
            Padding(
              padding:
                  EdgeInsets.symmetric(vertical: ScreenUtil().setHeight(3)),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    flex: 65,
                    child: BaseWidget.text('Tổng: ',
                        textSize: ScreenUtil().setSp(16),
                        fontWeight: FontWeight.w600),
                  ),
                  Expanded(
                      flex: 35,
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: BaseWidget.text(
                            '${Utils.formatMoney(double.parse(model.allInvoiceTotalPriceMap[date]))} đ',
                            textSize: ScreenUtil().setSp(16),
                            fontWeight: FontWeight.w600),
                      ))
                ],
              ),
            ),
            Padding(
              padding:
                  EdgeInsets.symmetric(vertical: ScreenUtil().setHeight(3)),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    flex: 65,
                    child: BaseWidget.text('Còn nợ: ',
                        textSize: ScreenUtil().setSp(16),
                        fontWeight: FontWeight.w600),
                  ),
                  Expanded(
                      flex: 35,
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: BaseWidget.text(
                            '${Utils.formatMoney(double.parse((int.parse(model.allInvoiceTotalPriceMap[date]) - int.parse(invoices[0].paid)).toString()))} đ',
                            textSize: ScreenUtil().setSp(16),
                            fontWeight: FontWeight.w600,
                            textColor: Colors.red),
                      ))
                ],
              ),
            ),
            Padding(
              padding:
                  EdgeInsets.symmetric(vertical: ScreenUtil().setHeight(3)),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    flex: 65,
                    child: BaseWidget.text('Đã trả: ',
                        textSize: ScreenUtil().setSp(16),
                        fontWeight: FontWeight.w600),
                  ),
                  Expanded(
                      flex: 35,
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: BaseWidget.text(
                            invoices[0].paid.contains('null')
                                ? "0 đ"
                                : '${Utils.formatMoney(double.parse(invoices[0].paid))} đ',
                            textSize: ScreenUtil().setSp(16),
                            fontWeight: FontWeight.w600,
                            textColor: Colors.green),
                      ))
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  _productInvoiceElementWidget(Invoice invoice) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: ScreenUtil().setHeight(3)),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Expanded(
              flex: 65,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  BaseWidget.text('${invoice.productName}',
                      textSize: ScreenUtil().setSp(18),
                      fontWeight: FontWeight.w600),
                  BaseWidget.text('Số lượng: ${invoice.count}'),
                ],
              )),
          Expanded(
              flex: 35,
              child: Align(
                alignment: Alignment.centerRight,
                child: BaseWidget.text(
                  '${Utils.formatMoney(double.parse(invoice.wholesale))} đ',
                  textColor: Colors.grey,
                ),
              ))
        ],
      ),
    );
  }
}
