import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:warehouse_of_pikata/enums/view_state.dart';
import 'package:warehouse_of_pikata/models/distributor.dart';
import 'package:warehouse_of_pikata/scoped_models/distributor_view_model.dart';
import 'package:warehouse_of_pikata/ui/view/base_view.dart';
import 'package:warehouse_of_pikata/ui/view/distributor/add_new_distributor_view.dart';
import 'package:warehouse_of_pikata/ui/widget/base_widget.dart';
import 'package:warehouse_of_pikata/ui/widget/loading/loading_view.dart';

class DistributorTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    ScreenUtil.init(context, width: 375, height: 812, allowFontScaling: true);
    return BaseView<DistributorViewModel>(onModelReady: (model) {
      model.getAllDistributors();
    }, builder: (context, child, model) {
      switch (model.state) {
        case ViewState.NoDataAvailable:
          return Scaffold(
            backgroundColor: Colors.transparent,
            appBar: mAppBar(context, model),
            body: Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  BaseWidget.text('Chưa có đối tác nào',
                      textColor: Colors.white54,
                      textSize: ScreenUtil().setSp(18)),
                  RaisedButton(
                    onPressed: () => Navigator.push(
                        context,
                        CupertinoPageRoute(
                            builder: (_) => AddNewDistributorView())),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(5))),
                    color: Colors.blue,
                    child: BaseWidget.text("Thêm",
                        textSize: ScreenUtil().setSp(18),
                        textColor: Colors.white),
                  )
                ],
              ),
            ),
          );
        case ViewState.Busy:
          return Scaffold(
            backgroundColor: Colors.transparent,
            appBar: mAppBar(context, model),
            body: Center(
              child: LoadingView(),
            ),
          );
        case ViewState.Success:
          return Scaffold(
            backgroundColor: Colors.transparent,
            appBar: mAppBar(context, model),
            body: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.vertical(top: Radius.circular(20)),
                  color: Colors.grey[100]),
              padding: EdgeInsets.only(top: ScreenUtil().setHeight(10)),
              child: _allContactsListView(context, model),
            ),
          );
        default:
          return Scaffold(
            backgroundColor: Colors.transparent,
            appBar: mAppBar(context, model),
            body: Center(
              child: BaseWidget.text('Lỗi',
                  textColor: Colors.white, textSize: ScreenUtil().setSp(18)),
            ),
          );
      }
    });
  }

  mAppBar(BuildContext context, DistributorViewModel model) {
    return AppBar(
      title: BaseWidget.text('Đối tác',
          textSize: ScreenUtil().setSp(20),
          textColor: Colors.white,
          fontWeight: FontWeight.w600),
      centerTitle: true,
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.filter_list,
            color: Colors.white,
          ),
          onPressed: () => model.changeView = !model.changeView,
        ),
        IconButton(
          icon: Icon(Icons.search, color: Colors.white),
        )
      ],
    );
  }

  Widget _allContactsListView(
      BuildContext context, DistributorViewModel model) {
    return ListView.builder(
        itemCount: model.allDistributors.length,
        itemBuilder: (context, index) {
          Distributor distributor = model.allDistributors[index];
          return Card(
            elevation: 3,
            color: Colors.white,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(8))),
            margin: EdgeInsets.only(
                left: ScreenUtil().setWidth(15),
                right: ScreenUtil().setWidth(15),
                bottom: ScreenUtil().setHeight(8)),
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: ScreenUtil().setWidth(10),
                  vertical: ScreenUtil().setHeight(8)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  BaseWidget.text('${distributor.name}',
                      textSize: ScreenUtil().setSp(18),
                      fontWeight: FontWeight.w600),
                  SizedBox(
                    height: ScreenUtil().setHeight(10),
                  ),
                  BaseWidget.text('${distributor.address}',
                      textSize: ScreenUtil().setSp(16),
                      fontWeight: FontWeight.w500,
                      textColor: Colors.black87,
                      maxLines: 2),
                  SizedBox(
                    height: ScreenUtil().setHeight(5),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      BaseWidget.text('Số điện thoại 1:\t',
                          textColor: Colors.grey),
                      InkWell(
                        onTap: () => _showContactDialog(context, model,
                            phone: distributor.phone1),
                        child: BaseWidget.text('${distributor.phone1}',
                            textColor: Colors.blue,
                            fontWeight: FontWeight.w600),
                      )
                    ],
                  ),
                  SizedBox(
                    height: ScreenUtil().setHeight(5),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      BaseWidget.text('Số điện thoại 2:\t',
                          textColor: Colors.grey),
                      InkWell(
                        onTap: () => _showContactDialog(context, model,
                            phone: distributor.phone2),
                        child: BaseWidget.text('${distributor.phone2}',
                            textColor: Colors.blue,
                            fontWeight: FontWeight.w600),
                      )
                    ],
                  ),
                ],
              ),
            ),
          );
        });
  }

  _showContactDialog(BuildContext context, DistributorViewModel model,
      {String phone}) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(8))),
            backgroundColor: Colors.grey[100],
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: ScreenUtil().setWidth(15),
                  vertical: ScreenUtil().setHeight(10)),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  InkWell(
                    onTap: () => launch("tel://$phone"),
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: ScreenUtil().setHeight(10)),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              flex: 15,
                              child:
                                  Icon(Icons.phone, color: Colors.grey[400])),
                          Expanded(flex: 85, child: BaseWidget.text("Gọi")),
                        ],
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () => launch("sms://$phone"),
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: ScreenUtil().setHeight(10)),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              flex: 15,
                              child:
                                  Icon(Icons.message, color: Colors.grey[400])),
                          Expanded(
                              flex: 85, child: BaseWidget.text("Nhắn tin")),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }
}
