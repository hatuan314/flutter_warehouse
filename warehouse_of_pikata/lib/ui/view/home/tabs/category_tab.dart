import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:warehouse_of_pikata/models/category.dart';
import 'package:warehouse_of_pikata/scoped_models/category/category_view_model.dart';
import 'package:warehouse_of_pikata/ui/view/base_view.dart';
import 'package:warehouse_of_pikata/ui/view/product/all_products_of_type_view.dart';
import 'package:warehouse_of_pikata/ui/widget/base_widget.dart';

class CategoryTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    ScreenUtil.init(context, width: 375, height: 812, allowFontScaling: true);

    return Scaffold(
      backgroundColor: Colors.transparent,
      appBar: BaseWidget.appBar('Danh mục'),
      body: BaseView<CategoryViewModel>(onModelReady: (model) {
        model.getAllCategories();
      }, builder: (context, child, model) {
        return Container(
          child: ListView.builder(
              itemCount: model.allCategories.length + 1,
              itemBuilder: (context, index) {
                if (index == model.allCategories.length) {
                  return _categoryElementWidget(
                      context, Category(id: '0', name: "Khác"));
                } else {
                  return _categoryElementWidget(
                      context, model.allCategories[index]);
                }
              }),
        );
      }),
    );
  }

  InkWell _categoryElementWidget(BuildContext context, Category category) {
    return InkWell(
      onTap: () =>
          Navigator.push(context,
              CupertinoPageRoute(
                  builder: (_) => AllProductsOfTypeView(category))),
      child: Card(
        elevation: 5,
        color: Colors.white,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10))),
        margin: EdgeInsets.symmetric(
            horizontal: ScreenUtil().setWidth(30),
            vertical: ScreenUtil().setHeight(8)),
        child: Container(
            padding: EdgeInsets.symmetric(vertical: ScreenUtil().setHeight(20)),
            alignment: Alignment.center,
            child: BaseWidget.text('${category.name}')),
      ),
    );
  }
}
