
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:warehouse_of_pikata/scoped_models/scan_barcode_view_model.dart';
import 'package:warehouse_of_pikata/ui/view/base_view.dart';
import 'package:warehouse_of_pikata/ui/view/category/add_new_category.dart';
import 'package:warehouse_of_pikata/ui/view/distributor/add_new_distributor_view.dart';
import 'package:warehouse_of_pikata/ui/view/invoice/add_new_invoice_view.dart';
import 'package:warehouse_of_pikata/ui/view/product/product_detail_view.dart';
import 'package:warehouse_of_pikata/ui/widget/base_widget.dart';


class MenuTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    ScreenUtil.init(context, width: 375, height: 812, allowFontScaling: true);
    return Scaffold(
      backgroundColor: Colors.transparent,
      appBar: BaseWidget.appBar('Menu'),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        alignment: Alignment.centerRight,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            InkWell(
              onTap: () => Navigator.push(context,
                  CupertinoPageRoute(builder: (_) => AddNewCategoryView())),
              child: _menuItemCard(
                  context, 'assets/icons/ic_add_list.png', 'Thêm danh mục'),
            ),
            InkWell(
              onTap: () => Navigator.push(context,
                  CupertinoPageRoute(builder: (_) => AddNewDistributorView())),
              child: _menuItemCard(
                  context, 'assets/icons/ic_add_contact.png', 'Thêm đối tác'),
            ),
            InkWell(
              onTap: () => Navigator.push(context,
                  CupertinoPageRoute(builder: (_) => AddNewInvoiceView())),
              child: _menuItemCard(
                  context, 'assets/icons/ic_invoice.png', 'Nhập hoá đơn'),
            ),
            BaseView<ScanBarcodeViewModel>(
              builder: (context, child, model) {
                return InkWell(
                  onTap: () async {
                    var data = await model.scanBarcode(context);
                    if(data != null) {
                      Navigator.push(context, CupertinoPageRoute(builder: (_) => ProductDetailView(data.name, data.categoryId)));
                    }
                  },
                  child: _menuItemCard(context, 'assets/icons/ic_barcode.png',
                      'Xem giá sản phẩm bằng barcode'),
                );
              }
            ),
          ],
        ),
      ),
    );
  }

  Widget _menuItemCard(BuildContext context, String iconPath, String text) {
    return Card(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.horizontal(left: Radius.circular(50))),
      elevation: 5,
      margin: EdgeInsets.symmetric(vertical: ScreenUtil().setHeight(8)),
      color: Colors.white10,
      child: Container(
        width: MediaQuery.of(context).size.width * 2 / 3,
        padding: EdgeInsets.symmetric(
            horizontal: ScreenUtil().setWidth(8),
            vertical: ScreenUtil().setHeight(10)),
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 25,
              child: Center(
                child: Image.asset(
                  iconPath,
                  fit: BoxFit.fill,
                  height: ScreenUtil().setHeight(35),
                  color: Colors.white,
                ),
              ),
            ),
            Expanded(
              flex: 75,
              child: BaseWidget.text(text,
                  textSize: ScreenUtil().setSp(16),
                  fontWeight: FontWeight.w600,
                  textColor: Colors.white,
                  maxLines: 2),
            )
          ],
        ),
      ),
    );
  }


}
