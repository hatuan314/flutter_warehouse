import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:warehouse_of_pikata/scoped_models/home_view_model.dart';
import 'package:warehouse_of_pikata/ui/view/base_view.dart';
import 'package:warehouse_of_pikata/ui/view/home/tabs/category_tab.dart';
import 'package:warehouse_of_pikata/ui/view/home/tabs/distributor_tab.dart';
import 'package:warehouse_of_pikata/ui/view/home/tabs/invoice_tab.dart';
import 'package:warehouse_of_pikata/ui/view/home/tabs/menu_tab.dart';
import 'package:warehouse_of_pikata/ui/widget/navigation_bar/ff_navigation_bar.dart';
import 'package:warehouse_of_pikata/ui/widget/navigation_bar/ff_navigation_bar_theme.dart';

class HomeView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    ScreenUtil.init(context, width: 375, height: 812, allowFontScaling: true);
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [Colors.blue[900], Colors.red[900]],
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          tileMode: TileMode.clamp,
        ),
      ),
      child: BaseView<HomeViewModel>(builder: (context, child, model) {
        return Scaffold(
          backgroundColor: Colors.transparent,
          body: _presentTab(model),
          bottomNavigationBar: FFNavigationBar(
            theme: FFNavigationBarTheme(
                barBackgroundColor: Colors.grey[100],
                selectedItemBorderColor: Colors.grey[100],
                selectedItemBackgroundColor: Colors.blue,
                selectedItemIconColor: Colors.grey[100],
                selectedItemLabelColor: Colors.blue,
                showSelectedItemShadow: false,
                selectedItemTextStyle: TextStyle(
                    fontFamily: 'QS',
                    fontSize: ScreenUtil().setSp(11),
                    fontWeight: FontWeight.w500)),
            selectedIndex: model.tabIndex,
            onSelectTab: (index) {
              model.tabIndex = index;
            },
            items: [
              FFNavigationBarItem(
                iconData: Icons.list,
                label: 'Danh mục',
              ),
              FFNavigationBarItem(
                iconData: Icons.assignment,
                label: 'Hoá đơn',
              ),
              FFNavigationBarItem(
                iconData: Icons.people,
                label: 'Đối tác',
              ),
              FFNavigationBarItem(
                iconData: Icons.apps,
                label: 'Menu',
              ),
            ],
          ),
        );
      }),
    );
  }

  Widget _presentTab(HomeViewModel model) {
    switch (model.tabIndex) {
      case 0:
        return CategoryTab();
      case 1:
        return InvoiceTab();
      case 2:
        return DistributorTab();
      case 3:
        return MenuTab();
      default:
        return CategoryTab();
    }
  }
}
