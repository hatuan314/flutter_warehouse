import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class Chart {
  static BarChartGroupData makeGroupData(int x, double y1, double y2) {
    return BarChartGroupData(barsSpace: 4, x: x, barRods: [
      BarChartRodData(
        y: y1,
        color: Color(0xff53fdd7),
        width: ScreenUtil().setWidth(7),
      ),
      BarChartRodData(
        y: y2,
        color: Color(0xffff5182),
        width: ScreenUtil().setWidth(7),
      ),
    ]);
  }
}
