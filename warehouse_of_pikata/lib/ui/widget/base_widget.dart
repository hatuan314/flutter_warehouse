import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class BaseWidget {
  static Text text(String title,
      {double textSize, Color textColor, FontWeight fontWeight, int maxLines}) {
    return Text(
      '$title',
      style: TextStyle(
        fontSize: textSize ?? ScreenUtil().setSp(16),
        color: textColor ?? Colors.black87,
        fontFamily: 'QS',
        fontWeight: fontWeight ?? FontWeight.w500,
      ),
      maxLines: maxLines ?? 1,
    );
  }

  static AppBar appBar(String title) {
    return AppBar(
      title: text('$title',
          textSize: ScreenUtil().setSp(20),
          textColor: Colors.white,
          fontWeight: FontWeight.w600),
      centerTitle: true,
      elevation: 0.0,
      backgroundColor: Colors.transparent,
    );
  }
}
