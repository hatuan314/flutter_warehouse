import 'package:flutter/material.dart';
import 'package:warehouse_of_pikata/database/category_service.dart';
import 'package:warehouse_of_pikata/enums/view_state.dart';
import 'package:warehouse_of_pikata/models/category.dart';
import 'package:warehouse_of_pikata/scoped_models/base_model.dart';
import 'package:warehouse_of_pikata/service_locator.dart';

class CategoryViewModel extends BaseModel {
  CategoryService _categoryService = locator<CategoryService>();

  /// Add New Category
  ViewState _addState;
  String _name;

  String get name => _name;

  set name(String value) {
    _name = value;
    notifyListeners();
  }

  ViewState get addState => _addState;

  set addState(ViewState value) {
    _addState = value;
    notifyListeners();
  }

  Future addNewCategory() async {
    addState = ViewState.Busy;
    try {
      var data = await _categoryService.createNewDistributor(this.name);
      if (data == false) {
        addState = ViewState.NotReady;
      } else {
        addState = ViewState.Success;
      }
    } catch (e) {
      debugPrint("CategoryViewModel - addNewCategory - Error: $e");
      addState = ViewState.Error;
    }
  }

  /// Select All Categories
  ViewState _getCategotyState;
  List<Category> _allCategories = List<Category>();

  List<Category> get allCategories => _allCategories;

  set allCategories(List<Category> value) {
    _allCategories = value;
    notifyListeners();
  }

  ViewState get getCategotyState => _getCategotyState;

  set getCategotyState(ViewState value) {
    _getCategotyState = value;
    notifyListeners();
  }

  Future getAllCategories() async {
    getCategotyState = ViewState.Busy;
    allCategories.clear();
    try {
      var data = await _categoryService.selectAllCategories();
      if (data == null) {
        allCategories = [];
        getCategotyState = ViewState.NoDataAvailable;
      } else {
//        allCategories = data;
        data.forEach((category) {
          allCategories.add(category);
        });
        getCategotyState = ViewState.DataFetched;
      }
    } catch (e) {
      debugPrint('ProductViewModel - getAllCategories - ERROR: $e');
      getCategotyState = ViewState.Error;
    }
  }
}
