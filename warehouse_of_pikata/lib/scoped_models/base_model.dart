import 'package:scoped_model/scoped_model.dart';
import 'package:warehouse_of_pikata/enums/view_state.dart';

import '../service_locator.dart';

class BaseModel extends Model {
  ViewState _state;
  int qtyCart = 0;
//  ShareService shareService = locator.get<ShareService>();

  String errorMessage;
  bool _reload = true;

  bool get reload => _reload;

  set reload(bool value) {
    _reload = value;
    notifyListeners();
  }

  ViewState get state => _state;

  void setState(ViewState newState) {
    _state = newState;
    // Notify listeners will only update listeners of state.
    notifyListeners();
  }
}
