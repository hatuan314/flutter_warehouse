import 'package:flutter/material.dart';
import 'package:warehouse_of_pikata/database/category_service.dart';
import 'package:warehouse_of_pikata/database/product_service.dart';
import 'package:warehouse_of_pikata/enums/view_state.dart';
import 'package:warehouse_of_pikata/models/category.dart';
import 'package:warehouse_of_pikata/models/product.dart';
import 'package:warehouse_of_pikata/scoped_models/base_model.dart';
import 'package:warehouse_of_pikata/service_locator.dart';

class ProductViewModel extends BaseModel {
  ProductService _productService = locator<ProductService>();
  bool _changeView = false;

  bool get changeView => _changeView;

  set changeView(bool value) {
    _changeView = value;
    notifyListeners();
  }

  /// Get All Products By CategoryId
  ViewState _getProductState;
  Map<String, List<Product>> _allProductsMap;

  Map<String, List<Product>> get allProductsMap => _allProductsMap;

  set allProductsMap(Map<String, List<Product>> value) {
    _allProductsMap = value;
    notifyListeners();
  }

  ViewState get getProductState => _getProductState;

  set getProductState(ViewState value) {
    _getProductState = value;
    notifyListeners();
  }

  Future getAllProducts(String categoryId) async {
    getProductState = ViewState.Busy;
    allProductsMap = Map<String, List<Product>>();
    try {
      var data =
      await _productService.selectAllProductsByCategoryId(categoryId);
      if (data == null || data.length == 0) {
        getProductState = ViewState.NoDataAvailable;
      } else {
        List<Product> allProducts = data;
        allProducts.forEach((product) {
          if (allProductsMap[product.name] == null) {
            allProductsMap[product.name] = [product];
          } else {
            allProductsMap[product.name].add(product);
          }
        });
        getProductState = ViewState.Success;
      }
    } catch (e) {
      debugPrint("ProductViewModel - getAllProducts - Error: $e");
      getProductState = ViewState.Error;
    }


  }

  /// Get Product Detail
  List<Product> _allProductsSameName;

  List<Product> get allProductsSameName => _allProductsSameName;

  set allProductsSameName(List<Product> value) {
    _allProductsSameName = value;
    notifyListeners();
  }

  Future getProductDetail(String name, String categoryId) async {
    setState(ViewState.Busy);
    allProductsSameName = List<Product>();
    try {
      var data = await _productService.selectProductDetail(name, categoryId);
      if (data == null || data.length == 0) {
        setState(ViewState.NoDataAvailable);
      } else {
        allProductsSameName = data;
        this.retail = allProductsSameName[0].retail.contains("null")
            ? "0"
            : allProductsSameName[0].retail;
        setState(ViewState.Success);
      }
    } catch (e) {
      debugPrint("ProductViewModel - getProductDetail - Error: $e");
      setState(ViewState.Error);
    }
  }

  /// Get All Categories
  CategoryService _categoryService = locator<CategoryService>();
  ViewState _getCategotyState;
  Map<String, Category> _allCategoriesMap;
  String _currentCategory;

  String get currentCategory => _currentCategory;

  set currentCategory(String value) {
    _currentCategory = value;
    notifyListeners();
  }

  Map<String, Category> get allCategoriesMap => _allCategoriesMap;

  set allCategoriesMap(Map<String, Category> value) {
    _allCategoriesMap = value;
    notifyListeners();
  }

  ViewState get getCategotyState => _getCategotyState;

  set getCategotyState(ViewState value) {
    _getCategotyState = value;
    notifyListeners();
  }

  Future getAllCategories(String categoryId) async {
    getCategotyState = ViewState.Busy;
    allCategoriesMap = Map<String, Category>();
    currentCategory = 'Khác';
    try {
      var data = await _categoryService.selectAllCategories();
      if (data == null) {
        allCategoriesMap = {};
        getCategotyState = ViewState.NoDataAvailable;
      } else {
        List<Category> allCategories = data;
        allCategories.forEach((category) {
          allCategoriesMap[category.name] = category;
          if (categoryId == category.id) {
            currentCategory = category.name;
          }
        });
        allCategoriesMap['Khác'] = Category(name: 'Khác', id: '0');
        allCategoriesMap['Thêm danh mục'] =
            Category(name: 'Thêm danh mục', id: '-1');

        getCategotyState = ViewState.DataFetched;
      }
    } catch (e) {
      debugPrint('ProductViewModel - getAllCategories - ERROR: $e');
      getCategotyState = ViewState.Error;
    }
  }

  /// Update Product
  ViewState _updateState;
  String _retail;

  ViewState get updateState => _updateState;

  set updateState(ViewState value) {
    _updateState = value;
    notifyListeners();
  }

  String get retail => _retail;

  set retail(String value) {
    _retail = value;
    notifyListeners();
  }

  Future updateProduct() async {
    updateState = ViewState.Busy;
    try {
      allProductsSameName.forEach((product) async {
        await _productService.updateCategoryIdAndRetail(
            allCategoriesMap[currentCategory].id, retail, product.name);
      });
      updateState = ViewState.Success;
    } catch (e) {
      debugPrint('ProductViewModel - updateProduct - Error: $e');
      updateState = ViewState.Error;
    }
  }
}
