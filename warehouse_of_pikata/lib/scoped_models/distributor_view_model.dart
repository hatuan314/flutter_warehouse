import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:warehouse_of_pikata/database/distributor_service.dart';
import 'package:warehouse_of_pikata/enums/view_state.dart';
import 'package:warehouse_of_pikata/models/distributor.dart';
import 'package:warehouse_of_pikata/scoped_models/base_model.dart';
import 'package:warehouse_of_pikata/service_locator.dart';

class DistributorViewModel extends BaseModel {
  DistributorService _service = locator<DistributorService>();

  bool _changeView = false;

  bool get changeView => _changeView;

  set changeView(bool value) {
    _changeView = value;
    notifyListeners();
  }

  /// INSERT NEW DISTRIBUTOR
  ///
  ///
  ViewState _createState = ViewState.WaitingForInput;
  String _name;
  String _colorId;
  String _address;
  String _phone1;
  String _phone2;
  String _color = '0';
  Color _distributorColor = Colors.blue;

  Color get distributorColor => _distributorColor;

  set distributorColor(Color value) {
    _distributorColor = value;
    notifyListeners();
  }

  String get color => _color;

  set color(String value) {
    _color = value;
    notifyListeners();
  }

  String get name => _name;

  set name(String value) {
    _name = value;
    notifyListeners();
  }

  String get colorId => _colorId;

  set colorId(String value) {
    _colorId = value;
    notifyListeners();
  }

  String get address => _address;

  set address(String value) {
    _address = value;
    notifyListeners();
  }

  String get phone1 => _phone1;

  set phone1(String value) {
    _phone1 = value;
    notifyListeners();
  }

  String get phone2 => _phone2;

  set phone2(String value) {
    _phone2 = value;
    notifyListeners();
  }

  ViewState get createState => _createState;

  set createState(ViewState value) {
    _createState = value;
    notifyListeners();
  }

  Future<void> addNewDistributor() async {
    createState = ViewState.Busy;
    Distributor distributor = Distributor(
        name: this.name,
        address: this.address,
        phone1: this.phone1,
        phone2: this.phone2,
        representColor: this.color);
    try {
      bool flag = await _service.createNewDistributor(distributor);
      if (flag == true) {
        createState = ViewState.Success;
      } else {
        createState = ViewState.NotReady;
      }
    } on Exception catch (e) {
      print('ERROR: $e');
      createState = ViewState.Error;
    }
  }

  /// SELECT ALL DISTRIBUTOR
  ///
  ///
  List<Distributor> _allDistributors = List<Distributor>();

  List<Distributor> get allDistributors => _allDistributors;

  set allDistributors(List<Distributor> value) {
    _allDistributors = value;
    notifyListeners();
  }

  Future getAllDistributors() async {
    setState(ViewState.Busy);
    try {
      var data = await _service.selectAllDistributor();
      if (data == null || data.length == 0) {
        setState(ViewState.NoDataAvailable);
      } else {
        allDistributors = data;
        setState(ViewState.Success);
      }
    } catch (e) {
      debugPrint('DistributorViewModel - getAllDistributors - ERROR: $e');
      setState(ViewState.Error);
    }
  }
}
