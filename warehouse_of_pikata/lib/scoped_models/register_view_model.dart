import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:flutter/services.dart';
import 'package:local_auth/local_auth.dart';
import 'package:warehouse_of_pikata/database/sign_in_service.dart';
import 'package:warehouse_of_pikata/enums/view_state.dart';
import 'package:warehouse_of_pikata/models/register.dart';
import 'package:warehouse_of_pikata/service_locator.dart';

import 'base_model.dart';

class RegisterViewModel extends BaseModel {
  SignInService _signInService = locator<SignInService>();
  var _key = utf8.encode('P1k4ta');

  String _username = '';
  String _password = '';

  bool _showPassword = false;

  bool get showPassword => _showPassword;

  set showPassword(bool value) {
    _showPassword = value;
    notifyListeners();
  }

  String get username => _username;

  set username(String value) {
    // HMAC-SHA256
    var hmacSha256 = new Hmac(sha256, _key);
    var usernameBytes = utf8.encode(value);

    var usernameDigest = hmacSha256.convert(usernameBytes);
    _username = usernameDigest.toString();
    notifyListeners();
  }

  String get password => _password;

  set password(String value) {
    var hmacSha256 = new Hmac(sha256, _key);
    var passwordBytes = utf8.encode(value);
    var passwordDigest = hmacSha256.convert(passwordBytes);
    _password = passwordDigest.toString();
    notifyListeners();
  }

  /// CHECK REGISTER DATA
  ///
  ///
  bool _isRegister = false;

  bool get isRegister => _isRegister;

  set isRegister(bool value) {
    _isRegister = value;
    notifyListeners();
  }

  Future<void> checkRegister() async {
    setState(ViewState.Busy);
    try {
      bool isRegister = await _signInService.checkRegisterData();
      this.isRegister = isRegister;
      setState(ViewState.Success);
    } catch (e) {
      print('error: $e');
      setState(ViewState.Error);
    }
  }

  /// SIGN UP
  ///
  ///
  ViewState _signInState = ViewState.WaitingForInput;

  ViewState get signInState => _signInState;

  set signInState(ViewState value) {
    _signInState = value;
    notifyListeners();
  }

  Future<bool> signUp() async {
    print('username: $username');
    signInState = ViewState.Busy;
    try {
      Register register =
          new Register(id: 1, username: this.username, password: this.password);

      var isCreate = await _signInService.createNewUser(register);
      if (isCreate == false) {
        signInState = ViewState.Error;
        return false;
      } else {
        signInState = ViewState.Success;
        return true;
      }
    } on Exception catch (e) {
      print('error: $e');
      signInState = ViewState.Error;
    }
  }

  /// SIGN IN
  ///
  ///
  Future<bool> signIn() async {
    print("HMAC digest as hex string: ${this.username}");
    print("HMAC digest as hex string: ${this.password}");
    signInState = ViewState.Busy;
    try {
      var result =
          await _signInService.checkAccount(this.username, this.password);
      if (result == false) {
        signInState = ViewState.NoDataAvailable;
        return false;
      } else {
        signInState = ViewState.Success;
        return true;
      }
    } on Exception catch (e) {
      print("Sign In Error: $e");
      signInState = ViewState.Error;
    }
  }

  /// BIOMETRIC AUTH
  ///
  ///
  final LocalAuthentication auth = LocalAuthentication();
  String _authorized = 'Not Authorized';
  bool _isAuthenticating = false;

  bool get isAuthenticating => _isAuthenticating;

  set isAuthenticating(bool value) {
    _isAuthenticating = value;
    notifyListeners();
  }

  Future<bool> checkBiometrics() async {
    bool flag = false;
    try {
      bool canCheckBiometrics = await auth.canCheckBiometrics;
      if (canCheckBiometrics) {
        var availableBiometrics = await getAvailableBiometrics();
        availableBiometrics.forEach((f) {
          if (f == BiometricType.fingerprint) {
            flag = true;
          } else {
            flag = false;
          }
        });
      }

      return flag;
    } on PlatformException catch (e) {
      print(e);
    }
  }

  Future<List<BiometricType>> getAvailableBiometrics() async {
    List<BiometricType> availableBiometrics = [];
    try {
      availableBiometrics = await auth.getAvailableBiometrics();
      return availableBiometrics;
    } on PlatformException catch (e) {
      print(e);
    }
  }

  Future<bool> biometricAuthenticate() async {
    try {
      bool authenticated = await auth.authenticateWithBiometrics(
          localizedReason: 'Scan your fingerprint to authenticate',
          useErrorDialogs: true,
          stickyAuth: true);
      return authenticated;
    } on PlatformException catch (e) {
      print(e);
      return false;
    }
  }

  String validateRequiredField(String text) {
    if (text.trim().isEmpty) {
      return "Trường này không được bỏ trống";
    }
    return null;
  }
}
