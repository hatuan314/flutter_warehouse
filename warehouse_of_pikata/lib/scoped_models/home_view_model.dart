import 'package:flutter/material.dart';

import 'base_model.dart';

class HomeViewModel extends BaseModel {
  int _tabIndex = 0;

  int get tabIndex => _tabIndex;

  set tabIndex(int value) {
    _tabIndex = value;
    notifyListeners();
  }
}
