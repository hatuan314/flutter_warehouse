import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:toast/toast.dart';
import 'package:warehouse_of_pikata/database/product_service.dart';
import 'package:warehouse_of_pikata/models/product.dart';
import 'package:warehouse_of_pikata/scoped_models/base_model.dart';
import 'package:warehouse_of_pikata/service_locator.dart';

class ScanBarcodeViewModel extends BaseModel {
  ProductService _productService = locator<ProductService>();
  Product _product;

  Product get product => _product;

  set product(Product value) {
    _product = value;
    notifyListeners();
  }

  Future<Product> scanBarcode(BuildContext context) async {
    try {
      product = Product();
      String barcode = await BarcodeScanner.scan();
      debugPrint('ScanBarcodeViewModel - scanBarcode: $barcode');
      var data = await _productService.selectProductByBarcode(barcode);
      if (data == null) {
        Toast.show(
            'Sản phẩm không tồn tại. Hãy nhập sản phẩm ở phần nhập hoá đơn',
            context,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            duration: Toast.LENGTH_LONG);
        return null;
      } else {
        product = data;
        return product;
      }
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
        Toast.show('The user did not grant the camera permission!', context,
            backgroundColor: Colors.red, textColor: Colors.white);
      } else {
        Toast.show('Unknown error: $e', context,
            backgroundColor: Colors.red, textColor: Colors.white);
      }
    } on FormatException {
      Toast.show(
          'null (User returned using the "back"-button before scanning anything. Result)',
          context,
          backgroundColor: Colors.red,
          textColor: Colors.white);
    } catch (e) {
      Toast.show('Unknown error: $e', context,
          backgroundColor: Colors.red, textColor: Colors.white);
    }
  }
}
