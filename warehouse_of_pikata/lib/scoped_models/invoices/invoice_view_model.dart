import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:warehouse_of_pikata/database/distributor_service.dart';
import 'package:warehouse_of_pikata/database/invoice_service.dart';
import 'package:warehouse_of_pikata/database/product_service.dart';
import 'package:warehouse_of_pikata/enums/view_state.dart';
import 'package:warehouse_of_pikata/models/distributor.dart';
import 'package:warehouse_of_pikata/models/invoice.dart';
import 'package:warehouse_of_pikata/models/product.dart';
import 'package:warehouse_of_pikata/scoped_models/base_model.dart';
import 'package:warehouse_of_pikata/service_locator.dart';

class InvoiceViewModel extends BaseModel {
  DateTime _invoiceDate;
  File _imageFile;
  int _timer;

  int get timer => _timer;

  set timer(int value) {
    _timer = value;
    notifyListeners();
  }

  DateTime get invoiceDate => _invoiceDate;

  set invoiceDate(DateTime value) {
    _invoiceDate = value;
    notifyListeners();
  }

  File get imageFile => _imageFile;

  set imageFile(File value) {
    _imageFile = value;
    notifyListeners();
  }

  /// SCAN BARCODE
  ///
  ///
  String _productName = '';
  String _count = '0';
  String _price = '';

  String get productName => _productName;

  set productName(String value) {
    _productName = value;
  }

  String get count => _count;

  set count(String value) {
    _count = value;
    notifyListeners();
  }

  String get price => _price;

  set price(String value) {
    _price = value;
    notifyListeners();
  }

  /// Select All Distributor
  DistributorService _distributorService = locator<DistributorService>();

  Map<String, Distributor> _allDistributorsMap = Map<String, Distributor>();
  List<String> _distributorsDropDown = List<String>();
  ViewState _getDistributorState;
  String _currentDistributor = '';

  String get currentDistributor => _currentDistributor;

  set currentDistributor(String value) {
    _currentDistributor = value;
    notifyListeners();
  }

  List<String> get distributorsDropDown => _distributorsDropDown;

  set distributorsDropDown(List<String> value) {
    _distributorsDropDown = value;
    notifyListeners();
  }

  ViewState get getDistributorState => _getDistributorState;

  set getDistributorState(ViewState value) {
    _getDistributorState = value;
    notifyListeners();
  }

  Map<String, Distributor> get allDistributorsMap => _allDistributorsMap;

  set allDistributorsMap(Map<String, Distributor> value) {
    _allDistributorsMap = value;
    notifyListeners();
  }

  Future getAllDistributors() async {
    getDistributorState = ViewState.Busy;
    productName = '';
    price = '';
    count = '';
    paid = 0;
    timer = 0;
    invoiceDate = DateTime.now();
    allDistributorsMap.clear();
    distributorsDropDown.clear();
    allInvoicesMap.clear();
    try {
      var data = await _distributorService.selectAllDistributor();
      if (data == null || data.length == 0) {
        allDistributorsMap = {};
        distributorsDropDown = [];
      } else {
        data.forEach((distributor) {
          allDistributorsMap[distributor.name] = distributor;
        });
        distributorsDropDown = allDistributorsMap.keys.toList();
      }
      distributorsDropDown.add("Thêm đối tác");
      currentDistributor = distributorsDropDown[0];

      int todayTimer = DateTime.parse(invoiceDate.toString().split(' ')[0])
          .millisecondsSinceEpoch;
//      debugPrint(
//          "InvoiceViewModel - getAllDistributors - todayTimer: $todayTimer");
      timer = invoiceDate.millisecondsSinceEpoch - todayTimer;
      invoiceDate = DateTime.parse(invoiceDate.toString().split(' ')[0]);
//      debugPrint(
//          "InvoiceViewModel - getAllDistributors - invoiceDate: $invoiceDate");
//      debugPrint("InvoiceViewModel - getAllDistributors - timer: $timer");
      getDistributorState = ViewState.Success;
    } catch (e) {
      debugPrint('InvoiceViewModel - getAllDistributors - ERROR: $e');
      getDistributorState = ViewState.Error;
    }
  }

  /// Select All Products
  ProductService _productService = locator<ProductService>();

  Map<String, Product> _allProdctsMap = Map<String, Product>();
  ViewState _getProductsState = ViewState.Busy;

  Map<String, Product> get allProdctsMap => _allProdctsMap;

  set allProdctsMap(Map<String, Product> value) {
    _allProdctsMap = value;
    notifyListeners();
  }

  ViewState get getProductsState => _getProductsState;

  set getProductsState(ViewState value) {
    _getProductsState = value;
    notifyListeners();
  }

  Future getAllProducts() async {
    getProductsState = ViewState.Busy;
    allProdctsMap.clear();
    try {
      var data = await _productService.selectAllProducts();
//      debugPrint('InvoiceViewModel - getAllProducts - data: ${data.length}');
      if (data == null || data.length == 0) {
        allProdctsMap = {};
      } else {
        for (int i = 0; i < data.length; i++) {
          Product product = data[i];
//          debugPrint(
//              'InvoiceViewModel - getAllProducts - product: ${product.count}');
          allProdctsMap[product.name] = product;
        }
//        debugPrint('InvoiceViewModel - getAllProducts - allProdctsMap: ${allProdctsMap.length}');
      }
      getProductsState = ViewState.Success;
    } catch (e) {
      debugPrint('InvoiceViewModel - getAllProducts - ERROR: $e');
      getProductsState = ViewState.Error;
    }
  }

  /// Insert Invoice
  InvoiceService _invoiceService = locator<InvoiceService>();
  ViewState _insertState;
  Map<int, Invoice> _allInvoicesMap = new Map<int, Invoice>();
  double _paid = 0;

  double get paid => _paid;

  set paid(double value) {
    _paid = value;
    notifyListeners();
  }

  Map<int, Invoice> get allInvoicesMap => _allInvoicesMap;

  set allInvoicesMap(Map<int, Invoice> value) {
    _allInvoicesMap = value;
    notifyListeners();
  }

  ViewState get insertState => _insertState;

  set insertState(ViewState value) {
    _insertState = value;
    notifyListeners();
  }

  Future addNewInvoice() async {
    insertState = ViewState.Busy;
    try {
      Distributor distributor = allDistributorsMap[currentDistributor];

      for (int i = 0; i < allInvoicesMap.length; i++) {
        Product newProduct;
        Product productInvoice = Product(
            name: allInvoicesMap[i].productName,
            count: allInvoicesMap[i].count,
            distributorId: distributor.id,
            barcode: allInvoicesMap[i].barcode,
            wholesale: allInvoicesMap[i].wholesale);
        var data = await _productService.selectAllProductsByName(
            productInvoice.name, distributor.id);
        if (data == null || data.length == 0) {
          debugPrint("InvoiceViewModel - addNewInvoice - New Product");
          var productRequest = await _createNewProduct(productInvoice);
          newProduct = productRequest;
        } else {
          newProduct = productInvoice;
        }

        debugPrint(
            "InvoiceViewModel - addNewInvoice - newProduct: ${json.encode(newProduct.toJson())}");
        Invoice invoice = Invoice(
          productId: newProduct.id,
          productName: newProduct.name,
          barcode: newProduct.barcode,
          distributorId: newProduct.distributorId,
          count: allInvoicesMap[i].count,
          paid: this.paid.toString(),
          wholesale: newProduct.wholesale,
          wholesaleTotal: (int.parse(allInvoicesMap[i].count) *
                  int.parse(newProduct.wholesale))
              .toString(),
          date: (this.invoiceDate.millisecondsSinceEpoch + timer).toString(),
        );
        var flag = await _invoiceService.createNewInvoice(invoice);
        if (flag == null || flag == false) {
          insertState = ViewState.NotReady;
        } else {
          insertState = ViewState.Success;
        }
      }
    } catch (e) {
      debugPrint('InvoiceViewModel - addNewInvoice - ERROR: $e');

      insertState = ViewState.Error;
    }
  }

  /// Create New Product

  Future<Product> _createNewProduct(Product product) async {
    try {
      var flag = await _productService.createNewProduct(product);
      if (flag == true) {
        var data = await _productService.selectAllProductsByName(
            product.name, product.distributorId);
        if (data != null && data.length > 0) {
          Product product = data[0];
//          debugPrint(
//              "InvoiceViewModel - _createNewProduct - product: ${product.id}");
          return product;
        }
      } else {
        insertState = ViewState.NotReady;
      }
    } catch (e) {
      debugPrint('InvoiceViewModel - createNewProduct - ERROR: $e');
      insertState = ViewState.Error;
    }
  }

  /// Get All Invoices
  ViewState _getInvoiceState;
  Map<int, List<Invoice>> _allInvoiceByDayMap = Map<int, List<Invoice>>();
  Map<int, String> _allInvoiceTotalPriceMap = Map<int, String>();

  Map<int, String> get allInvoiceTotalPriceMap => _allInvoiceTotalPriceMap;

  set allInvoiceTotalPriceMap(Map<int, String> value) {
    _allInvoiceTotalPriceMap = value;
    notifyListeners();
  }

  Map<int, List<Invoice>> get allInvoiceByDayMap => _allInvoiceByDayMap;

  set allInvoiceByDayMap(Map<int, List<Invoice>> value) {
    _allInvoiceByDayMap = value;
    notifyListeners();
  }

  ViewState get getInvoiceState => _getInvoiceState;

  set getInvoiceState(ViewState value) {
    _getInvoiceState = value;
    notifyListeners();
  }

  Future getAllInvoices() async {
    getInvoiceState = ViewState.Busy;
    allInvoiceByDayMap.clear();
    allInvoicesMap.clear();

    try {
      var data = await _invoiceService.selectAllInvoices();
      List<Invoice> allInvoices = data;
      if (allInvoices == null || allInvoices.length == 0) {
        getInvoiceState = ViewState.NoDataAvailable;
      } else {
        int totalPrice = 0;
        for (int index = 0; index < allInvoices.length; index++) {
          Invoice invoice = allInvoices[index];
          debugPrint(
              "InvoiceViewModel - getAllInvoices - invoice.date: ${invoice.date}");
          if (allInvoiceByDayMap[int.parse(invoice.date)] == null ||
              allInvoiceByDayMap[int.parse(invoice.date)].length == 0) {
            allInvoiceByDayMap[int.parse(invoice.date)] = List<Invoice>();
            allInvoiceByDayMap[int.parse(invoice.date)].add(invoice);
            totalPrice = 0;
          } else {
            allInvoiceByDayMap[int.parse(invoice.date)].add(invoice);
          }
          totalPrice += int.parse(invoice.count) * int.parse(invoice.wholesale);
          allInvoiceTotalPriceMap[int.parse(invoice.date)] =
              totalPrice.toString();
        }

        getInvoiceState = ViewState.Success;
      }
    } catch (e) {
      debugPrint('InvoiceViewModel - getAllInvoices - Error: $e');
      getInvoiceState = ViewState.Error;
    }


  }
}
