import 'package:warehouse_of_pikata/database/database.dart';

class Register {
  int id;
  String username;
  String password;

  Register({this.id, this.username, this.password});

  Register.fromJson(Map<String, dynamic> json) {
    this.id = json['id'];
    this.username = json['username'];
    this.password = json['password'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['username'] = this.username;
    data['password'] = this.password;
    return data;
  }
}
