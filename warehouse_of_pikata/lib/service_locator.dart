import 'package:get_it/get_it.dart';
import 'package:warehouse_of_pikata/database/distributor_service.dart';
import 'package:warehouse_of_pikata/database/invoice_service.dart';
import 'package:warehouse_of_pikata/database/product_service.dart';
import 'package:warehouse_of_pikata/scoped_models/category/category_view_model.dart';
import 'package:warehouse_of_pikata/scoped_models/distributor_view_model.dart';
import 'package:warehouse_of_pikata/scoped_models/invoices/invoice_view_model.dart';
import 'package:warehouse_of_pikata/scoped_models/scan_barcode_view_model.dart';
import 'package:warehouse_of_pikata/scoped_models/products/product_view_model.dart';
import 'package:warehouse_of_pikata/scoped_models/register_view_model.dart';

import 'database/category_service.dart';
import 'database/sign_in_service.dart';
import 'scoped_models/home_view_model.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  // Register services
  locator.registerLazySingleton<SignInService>(() => SignInService());
  locator.registerLazySingleton<DistributorService>(() => DistributorService());
  locator.registerLazySingleton<ProductService>(() => ProductService());
  locator.registerLazySingleton<InvoiceService>(() => InvoiceService());
  locator.registerLazySingleton<CategoryService>(() => CategoryService());

  // Register ScopedModels
  locator.registerLazySingleton<RegisterViewModel>(() => RegisterViewModel());
  locator.registerLazySingleton<HomeViewModel>(() => HomeViewModel());
  locator.registerLazySingleton<ProductViewModel>(() => ProductViewModel());
  locator.registerLazySingleton<DistributorViewModel>(
      () => DistributorViewModel());
  locator.registerLazySingleton<InvoiceViewModel>(() => InvoiceViewModel());
  locator.registerLazySingleton<CategoryViewModel>(() => CategoryViewModel());
  locator.registerLazySingleton<ScanBarcodeViewModel>(() => ScanBarcodeViewModel());
}
