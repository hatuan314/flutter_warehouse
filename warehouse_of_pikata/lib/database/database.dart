import 'dart:io';

import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

Database db;

class DatabaseCreator {
  static const registerTable = 'register';
  static const distributorTable = 'distributor';
  static const productTable = 'product';
  static const invoiceTable = 'invoice';
  static const categoryTable = 'category';

  static void databaseLog(String functionName, String sql,
      [List<Map<String, dynamic>> selectQueryResult,
      int insertAndUpdateQueryResult,
      List<dynamic> params]) {
    print(functionName);
    print(sql);
    if (params != null) {
      print(params);
    }
    if (selectQueryResult != null) {
      print(selectQueryResult);
    } else if (insertAndUpdateQueryResult != null) {
      print(insertAndUpdateQueryResult);
    }
  }

  Future createRegisterTable(Database db) async {
    final sql = '''CREATE TABLE $registerTable
    (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      username TEXT,
      password TEXT
    )''';

    await db.execute(sql);
  }

  Future createDistributorTable(Database db) async {
    final sql = '''CREATE TABLE $distributorTable
    (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      name TEXT,
      address TEXT,
      phone_1 TEXT,
      phone_2 TEXT,
      represent_color TEXT
    )''';

    await db.execute(sql);
  }

  Future createProductTable(Database db) async {
    final sql = '''CREATE TABLE $productTable
    (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      name TEXT,
      wholesale TEXT,
      retail TEXT,
      bar_code TEXT,
      category_id INTEGER,
      distributor_id INTEGER
    )''';

    await db.execute(sql);
  }

  Future createInvoiceTable(Database db) async {
    final sql = '''CREATE TABLE $invoiceTable
    (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      date INTEGER,
      product_id INTEGER,
      count INTEGER,
      paid INTEGER,
      wholesale TEXT,
      wholesale_total TEXT,
      distributor_id INTEGER
    )''';

    await db.execute(sql);
  }

  Future createCategoryTable(Database db) async {
    final sql = '''CREATE TABLE $categoryTable
    (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      category_name TEXT
    )''';

    await db.execute(sql);
  }

  Future<String> getDatabasePath(String dbName) async {
    final databasePath = await getDatabasesPath();
    final path = join(databasePath, dbName);

    //make sure the folder exists
    if (await Directory(dirname(path)).exists()) {
      //await deleteDatabase(path);
    } else {
      await Directory(dirname(path)).create(recursive: true);
    }
    return path;
  }

  Future<void> initDatabase() async {
    final path = await getDatabasePath('warehouse_db');
    db = await openDatabase(path, version: 1, onCreate: onCreate);
    print(db);
  }

  Future<void> onCreate(Database db, int version) async {
    await createRegisterTable(db);
    await createDistributorTable(db);
    await createProductTable(db);
    await createInvoiceTable(db);
    await createCategoryTable(db);
  }
}
