import 'package:warehouse_of_pikata/models/register.dart';

import 'database.dart';

class SignInService {
  Future<bool> checkAccount(String username, String password) async {
    //final sql = '''SELECT * FROM ${DatabaseCreator.AppNotiTable}
    //WHERE ${DatabaseCreator.id} = $id''';
    //final data = await db.rawQuery(sql);

    final sql = '''SELECT * FROM ${DatabaseCreator.registerTable}
    WHERE username = ? AND password = ?''';

    List<dynamic> params = [username, password];
    final data = await db.rawQuery(sql, params);
    print('data: $data');
    if (data == null || data.length == 0) {
      return false;
    } else {
      return true;
    }
  }

  Future<bool> checkRegisterData() async {
    final sql = '''SELECT * FROM ${DatabaseCreator.registerTable}''';

    final data = await db.rawQuery(sql);
    if (data == null || data.length == 0) {
      return false;
    } else {
      return true;
    }
  }

  Future<bool> createNewUser(Register register) async {
    final sql = '''INSERT INTO ${DatabaseCreator.registerTable}
    (
      username,
      password
    )
    VALUES (?,?)''';
    List<dynamic> params = [
      register.username,
      register.password,
    ];
    final result = await db.rawInsert(sql, params);
    print('data: $result');
    if (result == null || result == 0) {
      return false;
    } else {
      return true;
    }
    DatabaseCreator.databaseLog('Add account', sql, null, result, params);
  }

//  static Future<void> deleteAppNoti(AppNoti AppNoti) async {
//    /*final sql = '''UPDATE ${DatabaseCreator.AppNotiTable}
//    SET ${DatabaseCreator.isDeleted} = 1
//    WHERE ${DatabaseCreator.id} = ${AppNoti.id}
//    ''';*/
//
//    final sql = '''UPDATE ${DatabaseCreator.appNotiTable}
//    SET ${DatabaseCreator.isSeen} = 1
//    WHERE ${DatabaseCreator.id} = ?
//    ''';
//
//    List<dynamic> params = [AppNoti.id];
//    final result = await db.rawUpdate(sql, params);
//
//    DatabaseCreator.databaseLog('Delete AppNoti', sql, null, result, params);
//  }
//
//  static Future<void> updateAppNoti(AppNoti appNoti) async {
//    /*final sql = '''UPDATE ${DatabaseCreator.AppNotiTable}
//    SET ${DatabaseCreator.name} = "${AppNoti.name}"
//    WHERE ${DatabaseCreator.id} = ${AppNoti.id}
//    ''';*/
//
//    final sql = '''UPDATE ${DatabaseCreator.appNotiTable}
//    SET ${DatabaseCreator.isSeen} = 1
//    WHERE ${DatabaseCreator.id} = ?
//    ''';
//
//    List<dynamic> params = [appNoti.isSeen, appNoti.id];
//    final result = await db.rawUpdate(sql, params);
//
//    DatabaseCreator.databaseLog('Update AppNoti', sql, null, result, params);
//  }
//
//  static Future<int> AppNotisCount() async {
//    final data = await db
//        .rawQuery('''SELECT COUNT(*) FROM ${DatabaseCreator.appNotiTable}''');
//
//    int count = data[0].values.elementAt(0);
//    int idForNewItem = count++;
//    return idForNewItem;
//  }
}
