import 'package:flutter/material.dart';
import 'package:warehouse_of_pikata/models/category.dart';

import 'database.dart';

class CategoryService {
  /// Insert
  Future<bool> createNewDistributor(String name) async {
    final sql = '''INSERT INTO ${DatabaseCreator.categoryTable}
    (
      category_name
    )
    VALUES (?)''';
    List<dynamic> params = [
      name
    ];
    final request = await db.rawInsert(sql, params);
    debugPrint('CategoryService - createNewDistributor - request: $request');
    if (request == null || request == 0) {
      return false;
    } else {
      return true;
    }
//    DatabaseCreator.databaseLog('Add account', sql, null, result, params);
  }

  /// SELECT ALL CATEGORIES
  Future<List<Category>> selectAllCategories() async {
    final sql =
    '''SELECT * FROM ${DatabaseCreator
        .categoryTable} ORDER BY category_name ASC''';
    final request = await db.rawQuery(sql);
    List<Category> categories = List<Category>();
    for (final node in request) {
      Category category = Category.fromJson(node);
      categories.add(category);
    }

    return categories;
  }
}