import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:warehouse_of_pikata/database/database.dart';
import 'package:warehouse_of_pikata/models/product.dart';

class ProductService {
  /// Select All Products
  Future<List<Product>> selectAllProducts() async {
    final sql =
        '''SELECT * FROM ${DatabaseCreator.productTable} ORDER BY name ASC''';
    final request = await db.rawQuery(sql);
    List<Product> products = List<Product>();
    if (request == null || request.length == 0) {
      products = [];
    } else {
      for (final node in request) {
        Product product = Product.fromJson(node);
        products.add(product);
      }
    }

    return products;
  }

  /// Select All Product By CategoryId
  Future<List<Product>> selectAllProductsByCategoryId(
      String categoryId) async {
    final sql =
        '''SELECT * FROM ${DatabaseCreator.productTable} WHERE category_id = ? ORDER BY name ASC''';
    List<dynamic> params = [categoryId];
    final request = await db.rawQuery(sql, params);
    List<Product> products = List<Product>();
    if (request == null || request.length == 0) {
      products = [];
    } else {
      for (final node in request) {
//        debugPrint("ProductService - selectAllProductsByCategoryId - productNode: $node");
        Product product = Product.fromJson(node);
        products.add(product);
      }
    }

    return products;
  }

  /// Select All Product By Product Name And Distributor Id
  Future<List<Product>> selectAllProductsByName(
      String name, String distributorId) async {
    final sql =
        '''SELECT * FROM ${DatabaseCreator.productTable} WHERE name = ? AND distributor_id = ? ORDER BY id ASC''';
    List<dynamic> params = [name, distributorId];
    final request = await db.rawQuery(sql, params);
    List<Product> products = List<Product>();
    if (request == null || request.length == 0) {
      products = [];
    } else {
      for (final node in request) {
//        debugPrint(
//            "ProductService - selectAllProductsByCategoryId - selectAllProductsByName: $node");
        Product product = Product.fromJson(node);
        products.add(product);
      }
    }

    return products;
  }

  /// Select All Products By Product Name And Category Id
  Future<List<Product>> selectProductDetail(
      String name, String categoryId) async {
    final sql = '''SELECT
      ${DatabaseCreator.productTable}.id AS id,
      ${DatabaseCreator.productTable}.name AS name,
      ${DatabaseCreator.productTable}.wholesale AS wholesale,
      ${DatabaseCreator.productTable}.retail AS retail,
      ${DatabaseCreator.productTable}.bar_code AS bar_code,
      ${DatabaseCreator.productTable}.category_id AS category_id,
      ${DatabaseCreator.productTable}.distributor_id AS distributor_id,
      ${DatabaseCreator.distributorTable}.name AS distributor_name,
      ${DatabaseCreator.distributorTable}.represent_color AS represent_color
      FROM ${DatabaseCreator.productTable}
      INNER JOIN ${DatabaseCreator.distributorTable} ON ${DatabaseCreator.distributorTable}.id = ${DatabaseCreator.productTable}.distributor_id
      WHERE ${DatabaseCreator.productTable}.name = ? AND category_id = ? ORDER BY ${DatabaseCreator.distributorTable}.name ASC''';
    List<dynamic> params = [name, categoryId];
    final request = await db.rawQuery(sql, params);
    List<Product> products = List<Product>();
    if (request == null || request.length == 0) {
      products = [];
    } else {
      for (final node in request) {
        debugPrint(
            "ProductService - selectProductDetail: $node");
        Product product = Product.fromJson(node);
        products.add(product);
      }
    }

    return products;
  }

  /// Select Product By Id
  Future<List<Product>> selectProductById(String id) async {
    final sql = '''SELECT * FROM ${DatabaseCreator.productTable} WHERE id = ?''';
    List<dynamic> params = [id];
    final request = await db.rawQuery(sql, params);
    List<Product> products = List<Product>();
    if (request == null || request.length == 0) {
      products = [];
    } else {
      for (final node in request) {
        debugPrint(
            "ProductService - selectProductById: $node");
        Product product = Product.fromJson(node);
        products.add(product);
      }
    }

    return products;
  }

  /// Select Product By Barcode
  Future<Product> selectProductByBarcode(String barcode) async {
    final sql = '''SELECT * FROM ${DatabaseCreator.productTable} WHERE bar_code = ?''';
    List<dynamic> params = [barcode];
    final request = await db.rawQuery(sql, params);
    Product product;
    if (request == null || request.length == 0) {
      product = null;
    } else {
      for (final node in request) {
        debugPrint(
            "ProductService - selectProductByBarcode: $node");
        product = Product.fromJson(node);
      }
    }

    return product;
  }
  /// INSERT NEW PRODUCT
  Future<bool> createNewProduct(Product product) async {
    debugPrint('ProductService - createNewProduct: product: ${json.encode(product.toJson())}');
    final sql = '''INSERT INTO ${DatabaseCreator.productTable}
    (
      name,
      category_id,
      wholesale,
      bar_code,
      distributor_id
    )
    VALUES (?,?,?,?,?)''';
    List<dynamic> params = [
      product.name,
      '0',
      product.wholesale,
      product.barcode,
      product.distributorId
    ];
    final request = await db.rawInsert(sql, params);
    if (request == null || request == 0) {
      return false;
    } else {
      return true;
    }
//    DatabaseCreator.databaseLog('Add account', sql, null, result, params);
  }

  /// Update A Product Count
  Future<bool> updateProduct(Product product) async {
    final sql = '''UPDATE ${DatabaseCreator.productTable}
    SET count = ?
    WHERE id = ? AND distributor_id = ?
    ''';

    List<dynamic> params = [product.count, product.id, product.distributorId];
    final result = await db.rawUpdate(sql, params);

    DatabaseCreator.databaseLog('Update Product', sql, null, result, params);
  }

  /// Update A Product CategoryId And Retail
  Future<bool> updateCategoryIdAndRetail(String categoryId, String retail, String name) async {
    final sql = '''UPDATE ${DatabaseCreator.productTable}
    SET category_id = ?, retail = ?
    WHERE name = ?
    ''';

    List<dynamic> params = [categoryId, retail, name];
    final result = await db.rawUpdate(sql, params);

    DatabaseCreator.databaseLog('Update Product', sql, null, result, params);
  }
}
