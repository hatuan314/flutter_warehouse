import 'package:flutter/cupertino.dart';
import 'package:warehouse_of_pikata/models/distributor.dart';

import 'database.dart';

class DistributorService {

  /// Insert
  Future<bool> createNewDistributor(Distributor distributor) async {
    final sql = '''INSERT INTO ${DatabaseCreator.distributorTable}
    (
      name,
      address,
      phone_1,
      phone_2,
      represent_color
    )
    VALUES (?,?,?,?,?)''';
    List<dynamic> params = [
      distributor.name,
      distributor.address,
      distributor.phone1,
      distributor.phone2,
      distributor.representColor
    ];
    final request = await db.rawInsert(sql, params);
    debugPrint('DistributorService - createNewDistributor - request: $request');
    if (request == null || request == 0) {
      return false;
    } else {
      return true;
    }
//    DatabaseCreator.databaseLog('Add account', sql, null, result, params);
  }

  /// Select All
  Future<List<Distributor>> selectAllDistributor() async {
    final sql = '''SELECT * FROM ${DatabaseCreator.distributorTable} ORDER BY name ASC''';
    final request = await db.rawQuery(sql);

    List<Distributor> allDistributors = List<Distributor>();

    for(final distributorNode in request) {
      Distributor distributor = Distributor.fromJson(distributorNode);
      allDistributors.add(distributor);
    }
    debugPrint('DistributorService - selectAllDistributor - allDistributors: ${allDistributors.length}');
    return allDistributors;
  }
}
