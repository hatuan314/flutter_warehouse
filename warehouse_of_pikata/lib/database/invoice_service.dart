import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:warehouse_of_pikata/database/database.dart';
import 'package:warehouse_of_pikata/models/invoice.dart';

class InvoiceService {
  /// Insert Invoice
  Future<bool> createNewInvoice(Invoice invoice) async {
//    debugPrint('InvoiceService - createNewInvoice - invoice: ${json.encode(invoice.toJson())}');
    final sql = '''INSERT INTO ${DatabaseCreator.invoiceTable}
    (
      date,
      product_id,
      count,
      distributor_id,
      paid,
      wholesale,
      wholesale_total
    )
    VALUES (?,?,?,?,?,?,?)''';
    List<dynamic> params = [
      invoice.date,
      invoice.productId,
      invoice.count,
      invoice.distributorId,
      invoice.paid,
      invoice.wholesale,
      invoice.wholesaleTotal
    ];
    final request = await db.rawInsert(sql, params);
    debugPrint('InvoiceService - createNewInvoice - request: $request');
    if (request == null || request == 0) {
      return false;
    } else {
      return true;
    }
//    DatabaseCreator.databaseLog('Add account', sql, null, result, params);
  }

  /// Select All Invoice
  Future<List<Invoice>> selectAllInvoices() async {
    final sql = '''SELECT
      ${DatabaseCreator.invoiceTable}.id AS id,
      ${DatabaseCreator.invoiceTable}.date AS date,
      ${DatabaseCreator.invoiceTable}.product_id AS product_id,
      ${DatabaseCreator.invoiceTable}.count AS count,
      ${DatabaseCreator.invoiceTable}.wholesale_total AS wholesale_total,
      ${DatabaseCreator.productTable}.name AS product_name,
      ${DatabaseCreator.invoiceTable}.paid AS paid,
      ${DatabaseCreator.invoiceTable}.wholesale AS wholesale,
      ${DatabaseCreator.invoiceTable}.distributor_id AS distributor_id,
      ${DatabaseCreator.distributorTable}.name AS distributor_name,
      ${DatabaseCreator.distributorTable}.represent_color AS represent_color
      FROM ${DatabaseCreator.invoiceTable}
      INNER JOIN ${DatabaseCreator.distributorTable} ON ${DatabaseCreator.distributorTable}.id = ${DatabaseCreator.invoiceTable}.distributor_id
      INNER JOIN ${DatabaseCreator.productTable} ON ${DatabaseCreator.productTable}.id = ${DatabaseCreator.invoiceTable}.product_id
      ORDER BY date DESC''';
    final request = await db.rawQuery(sql);

    List<Invoice> allInvoices = List<Invoice>();

    for(final invoicerNode in request) {
      debugPrint("InvoiceService - selectAllInvoices - invoiceNode: $invoicerNode");
      Invoice invoice = Invoice.fromJson(invoicerNode);
      allInvoices.add(invoice);
    }
    return allInvoices;
  }
}
